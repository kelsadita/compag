<?php

	/**
	 * 
	 */
	class Videos extends CI_Controller {
		
		public function __construct() {
			
			parent::__construct();
			$this->load->model('videos_model');
			$this->load->model('login_model');
			$this->load->model('update_model');
			$this->load->model('profile_model');
			$this->load->library('session');
			$this->load->helper('form');
			$this->load->helper('url');
		}
		
		public function index()
		{
			$user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'Videos';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$data['videos_info'] = $this->videos_model->get_videos_info();
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/videos/videos_view', $data);
			$this->load->view('templates/footer');
			
		}
		
		public function add()
		{
			$user_id = $this->session->userdata('user_id');
			
			if(isset($_POST['share']))
			{
    			$video_title = $this->input->post('video_title');
    			$url = $this->input->post('url');
        
    			$url_array = explode('&',$url);
    			$raw_url = $url_array[0];
    			$url_array2 = explode('=', $raw_url);
    			$url_to_check = $url_array2[0];
    			$embed_url = end($url_array2);
    
    			$youtube_http = "http://www.youtube.com/watch?v";
    			$youtube_https = "https://www.youtube.com/watch?v";
    
    			if($youtube_http == $url_to_check || $youtube_https == $url_to_check)
    			{
    				if($video_title && $url)
    				{
        				
        				$this->videos_model->set_video($user_id, $video_title, $embed_url);
						$this->update_model->update($user_id, "videos", "video_id");
						$this->session->set_flashdata('notice', 'the video has been added successfully !');
						redirect('videos');
    				}

    			}
    			else
   	 			{
        			$this->session->set_flashdata('notice', 'URL must be from youtube !');
					redirect('videos');    
    			}
			}
		}
	}
	
?>