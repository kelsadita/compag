<?php
		
	/**
	 * 
	 */
	class Login extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->model('login_model');
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->library('email');
		}
		
		public function index($redirect_url = NULL)
		{
			$this->load->helper('form');
			$this->load->helper('url');
			
			if($this->session->userdata('user_id'))
			{
				redirect('home');
			}
			
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$data['title'] = 'Login';
			
			//----------------------------------------------------------------
			//Dont touch it huston!!!
			
			if($redirect_url !== NULL)
			{
				$data['error'] = "Huston!!! you need to log in to see the required page - kelsadita";
			}
			
			if(!$this->session->userdata('red_url'))
			{
				$this->session->set_userdata('red_url',urldecode($redirect_url)); //redirect(urldecode($redirect_url));
			}
			
			//---------------------------------------------------------------- 
			
			if($_POST)
			{
				if(empty($email) || empty($password))
				{
					$data['error'] = 'You must enter email and password !';
					$action_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
					$data['action_url'] = $action_url;
					
					$this->load->view('templates/header',$data);
					$this->load->view('pages/login_view',$data);
					$this->load->view('templates/footer');
				}
				else
				{
					$isPresent = $this->login_model->isNotConfirmed($email, $password);
					if($isPresent == 1)
					{
						$this->session->set_flashdata('notice', 'You need to confirm your email id. Please check your mail inbox or spam folder !');
						redirect(base_url().'login');
					}
					$count = $this->login_model->isAuth($email, $password);
					if($count == 1)
					{
						$user_info = $this->login_model->get_info($email);
						$this->session->set_userdata($user_info);
						$red_url = $this->session->userdata('red_url');
						$this->session->unset_userdata('red_url');
						redirect($red_url);
						
					}	
					else 
					{
						
						$data['error'] = 'Email and Password you entered is incorrect !';
						$this->load->view('templates/header', $data);
						$this->load->view('pages/login_view',$data);
						$this->load->view('templates/footer');
					}	
				}
			}
			else
			{
				
				
				//$action_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				//$data['action_url'] = $action_url;

				$this->load->view('templates/header', $data);
				$this->load->view('pages/login_view', $data);
				$this->load->view('templates/footer');
			}
		}
		
		public function verify()
		{
			
			$data['title'] = "Login";
			
			$this->load->view('templates/header', $data);
			$this->load->view('pages/verification_view');
			$this->load->view('templates/footer');	
		}
		
		public function remote_login($key = 0)
		{
			
				if($this->login_model->is_privatekey_authenticated($key) == 1)
				{
					
					
					if($this->session->userdata('pub_key')){
					
						$key = $this->session->userdata('pub_key');
						$data['key'] = $key;
						$data['urlShrtKey'] = $this->session->userdata('urlShrtKey');
						$data['title'] = "Auth Login";
				
						$this->load->view('templates/header', $data);
						$this->load->view('pages/remote_login_view');
						$this->load->view('templates/footer');
					}else{
						$data['title'] = "Auth Login";
						$this->load->view('templates/header', $data);
						$this->load->view('pages/unauth_login_view');
						$this->load->view('templates/footer');
					}
							
				}
				else
				{
					$this->load->view('pages/error_view');
				}
			
		}
		
		public function check($key)
		{			
			$user_id = $this->login_model->get_remote_userid($key);
		
					if($user_id != 0)
					{
						$user_info = $this->login_model->get_user_info($user_id);
						$this->session->set_userdata($user_info);
						$this->session->set_userdata('user_id', $user_id);
						$this->login_model->delete_remote_key($key);
						echo '{"result" : "1"}';
					}else{
						echo '{"result" : "0"}';
					}
		}
		
		public function mlogin($key, $pid = 0)
		{
		
		if(strlen($key) == 5){
			
			$public_key = $this->login_model->get_public_key($key);
			redirect('http://www.compag.in/login/mlogin/'.$public_key);
		}
		
		if($pid == 0){
		if($this->login_model->is_publickey_authenticated($key) == 1){
			if(isset($_POST['submit']))
			{
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				
				if(empty($email) || empty($password))
				{
					$data['notice'] = 'You must enter email and password !';	
					
				}
				else
				{
					$isPresent = $this->login_model->isNotConfirmed($email, $password);
					if($isPresent == 1)
					{
						$data['notice'] = 'You need to confirm your email id. Please check your mail inbox or spam folder !';
						
					}
					$count = $this->login_model->isAuth($email, $password);
					if($count == 1)
					{
						$data['userdata'] = $this->login_model->get_info($email);
						$user_id = $data['userdata']['user_id'];
						$this->login_model->updt_romote_login($key, $user_id);
						
						$data['notice'] = 'success';
							
					}	
					else 
					{
						$data['notice'] = 'Email and Password you entered is incorrect !';
						
				
						
					}	
				}
			}
			else
			{
				$data['notice'] = ' ';
			}
			$data['key'] = $key;
			$this->load->view('pages/mobile_login_view', $data);
			}
			else
			{
				$this->load->view('pages/error_view');
			}
			}
			else
			{
				if($this->login_model->is_publickey_authenticated($key) == 1){
				
					$this->login_model->updt_romote_login($key, $pid);
					echo '{"Auth" : "1"}';
				}else{
					echo '{"Auth" : "0"}';
				}
			}
			
		}
		
		public function mob_app_login($raw_email, $hash)//, $salt)
		{
				$email = urldecode($raw_email);
				$password = urldecode($hash);
				
				/******** Fixing the bug caused due to kodingen shutdown ********
				
				$usr_data = $this->login_model->get_hash_password($email);
				$password = $usr_data['password'];
				$password = urlencode($password);
    				
				//echo $password."<br/>";
				$json_url = "http://kelsadita.kodingen.com/demo.php?password=$password&salt=$salt";
				 
				// jSON String for request
				
				 
				// Initializing curl
				$ch = curl_init( $json_url );
				 
				// Configuring curl options
				$options = array(
				CURLOPT_RETURNTRANSFER => true,
			
				CURLOPT_HTTPHEADER => array('Content-type: application/json') 
				
				);
				 
				// Setting curl options
				curl_setopt_array( $ch, $options );
				 
				// Getting results
				$hashed_password =  curl_exec($ch); // Getting jSON result string
			
				$data = json_decode($hashed_password,true);						
				$hashed_password = $data['result'];
				//$hashed_password = crypt($password, $salt);
				$hash = urldecode($hash);
				
				//echo "salt is : ".urldecode($salt)."<br/>";
				//echo "hashed password is : ".$hashed_password."<br/>";
				//echo "hash is : ".$hash."<br/>";
		
		
				if($hash == $hashed_password){
					$authenticated = 1;
				}else{
					$authenticated = 0;
				}
				*/
				//$password = urldecode($hash);
				
				//checkcing email id and password combination is authenticated or not.
				
				$count = $this->login_model->isAuth_for_mobile_app($email, $password);
				//echo $count;
				//Count == 1  for correct email-id and password combination.
				if( $count == 1 ){
					$data['userdata'] = $this->login_model->get_info($email);
					$user_id = $data['userdata']['user_id'];
					$username = $data['userdata']['username'];
					//$this->login_model->updt_romote_login($key, $user_id);
					echo '{
						"Auth" : "1",
						"pid" : "'.$user_id.'",
						"username" : "'.$username.'"
						}';
				}else {
					$isPresent = $this->login_model->isNotConfirmed($email, $password);
					if($isPresent == 1){
						echo '{
						"Auth" : "2",
						"pid" : "0"
						}';
						
					}else{
					echo '{
						"Auth" : "0",
						"pid" : "0"
						}';
					}
				}	
		
			
			
		}

		public function logout()
		{

			$this->load->helper('url');
	
			$user_data = $this->session->all_userdata();
			$this->session->unset_userdata($user_data);
					
			$this->session->sess_destroy();
			
			
			redirect('login');
		}
		
		public function reset()
		{
			$data['title'] = 'Forgot Password';
			if(isset($_POST['submit']))
			{
				$to = $this->input->post('email');
			
				$num_users = $this->login_model->user_count($to);
				
			
				if($num_users != 1)
				{
					$this->session->set_flashdata('notice', 'Please enter your compag email id.');
					redirect(base_url().'login/reset');
				}
				else
				{
					$length = 10;
					$characters = '123456789abcdefghijklmnopqrstuvwxyz';
					$string = '';    
					for ($p = 0; $p < $length; $p++) 
					{
						$string .= $characters[mt_rand(0, strlen($characters)-1)];
					}
					
					$confir_id = $string;
					
					$success = $this->login_model->set_resetpassdata($to, $confir_id);
					
					if($success == 0)
					{
						$this->email->from('admin@compag.in', 'Compag Admin');
						
						$this->email->to($to); 

						$this->email->subject('Reset Password');
						$this->email->message('Click this link to reset your password http://www.compag.in/login/change_password/'.$confir_id);	

						
						if($this->email->send())
						{
							$this->session->set_flashdata('notice', 'check your mail inbox as well as spam folder for reset password link !');
							redirect(base_url().'login/reset');	
						}
						else
						{
							$this->session->set_flashdata('notice', 'mail sending problem !');
							redirect(base_url().'login/reset');
						}
						
					
					}
					else
					{
						$this->session->set_flashdata('notice', 'Your request has been already submitted.');
						redirect(base_url().'login/reset');
					}
				}
			}
		
		
	
			$this->load->view('templates/header', $data);
			$this->load->view('pages/fpass_view');
			$this->load->view('templates/footer');
		}
		
		public function change_password($key)
		{
			
			$data['title'] = 'Reset Password';
			$data['key'] = $key;
			
			$email = $this->login_model->reset_pass_data($key);
			if($email == 'nodata')
			{
				redirect(base_url()."login");
			}
			
			$this->load->view('templates/header', $data);
			$this->load->view('pages/reset_password_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function updt_password($key)
		{
			if(!empty($key))
			{
				$email = $this->login_model->reset_pass_data($key);
			
				if(isset($_POST['submit']))
				{
					$password1 = $this->input->post('password1');
					$password2 = $this->input->post('password2');					
					if($password1 == $password2)
					{
						$query = $this->login_model->reset_password($password1, $email);
						$this->session->set_flashdata('notice', 'the password has been changed successfully !');
						$this->login_model->delete_key($key);
						redirect(base_url()."login");
					}
					else
					{
						$this->session->set_flashdata('notice', 'the passwords you entered are not matching !');
						redirect(base_url()."login");
					}
				}
				
			}
			else
			{
				redirect(base_url()."login");
			}

		}
		
		public function register()
		{
			$data['title'] = 'Signup';
			
			if(isset($_POST['submit']))
			{
				$firstname = $this->input->post('firstname');
				$lastname = $this->input->post('lastname');
				$username = $firstname." ".$lastname;
				
				$password1 = $this->input->post('password1');
				$password2 = $this->input->post('password2');
				
				if($password1 != $password2)
				{
					$this->session->set_flashdata('notice', 'the passwords you entered are not matching !');
					redirect(base_url().'login/register');
				}
				else
				{
					$email = $this->input->post('email');
					$gender = $this->input->post('gender');
					$birthdate = $this->input->post('birthdate');
					
					$isEmailPresent = $this->login_model->chk_email($email);
					if($isEmailPresent == 1)
					{
						$this->session->set_flashdata('notice', 'the account with this email id already present !');
						redirect(base_url().'login/register');
					}
				
					
					$this->login_model->add_user($username, $password1, $email, $gender, $birthdate);
					
					//-----------------------------------------------------------------------------------------------------------------
					$length = 10;
					$characters = '123456789abcdefghijklmnopqrstuvwxyz';
					$string = '';    
					for ($p = 0; $p < $length; $p++) 
					{
						$string .= $characters[mt_rand(0, strlen($characters)-1)];
					}
					
					$key = $string;
					
					//Adding user confirmation in request table
					$this->login_model->add_confirmation($email, $key);
										
					$this->email->from('admin@compag.in', 'Compag Admin');
					$this->email->to($email); 
					$this->email->subject('Account Confirmation Of Compag');
					$message = "Dear user,\nYou have been confirmed to signup into compag.in \nPlease click this link http://www.compag.in/login/confirm/".$key;
					$this->email->message($message);
					$this->email->send();	
					//------------------------------------------------------------------------------------------------------------------
					
					$this->session->set_flashdata('notice', 'you have been successfully registered to compag please check your mail to confirm your email id');
					redirect(base_url().'login/register');
				}
			}
			
			$this->load->view('templates/header', $data);
			$this->load->view('pages/signup_view');
			$this->load->view('templates/footer');	
		}
		
		public function confirm($key)
		{
			$isPresent = $this->login_model->chk_user($key);
			
			if($isPresent == 1)
			{
				$this->session->set_flashdata('notice', 'your account has been confirmed you can login now !');
	
			}
			else
			{
				$this->session->set_flashdata('notice', 'invalid request !');
			}
			redirect(base_url().'login');
		}
	}
	
?>