<?php

	/**
	 * 
	 */
	class Semester extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->model('sem_model');
			$this->load->model('login_model');
			$this->load->model('update_model');
			$this->load->library('session');
			$this->load->helper('email_update');
			$this->load->library('email');
			$this->load->helper('form');
			$this->load->helper('url');
			$this->load->helper('download');
		}
		
		public function index()
		{
			$user_id = $this->session->userdata('user_id');
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$sem =$this->sem_model->get_sem($user_id);
			
			if($sem == 0)
			{
				redirect(base_url()."semester/update/".$user_id);
			}
			
			$data['title'] = 'Semester';
			$data['user_id'] = $user_id;
			$data['sem_id'] = $sem;
			$data['notice_data'] = $this->sem_model->get_notice($sem);
			$data['subjects_data'] = $this->sem_model->get_subjects($sem);
			
			$this->load->view('templates/header', $data);
			$this->load->view('semester/sem_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function update($user_id)
		{
			$user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'Semester';
			$data['user_id'] = $user_id;
			
			$this->load->view('templates/header', $data);
			$this->load->view('semester/updt_sem_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function update_sem()
		{
			$user_id = $this->session->userdata('user_id');
			$this->sem_model->update_sem($user_id);
			redirect(base_url()."semester");
		}

		public function timetable()
		{
			$user_id = $this->session->userdata('user_id');
			$sem =$this->sem_model->get_sem($user_id);
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			
			$data['title'] = 'Timetable';
			$data['sem_id'] = $sem;
			$data['user_id'] = $user_id;
			$data['tt_data'] = $this->sem_model->get_timetable($sem);
			
							
			$this->load->view('templates/header', $data);
			$this->load->view('semester/timetable_view', $data);
			$this->load->view('templates/footer');
		}

		public function update_tt($sem_id)
		{
			$success = $this->sem_model->update_tt($sem_id);
			if($success == 1)
			{
				$this->session->set_flashdata('notice', 'Timetable has been updated successfully !');
			}
			else
			{
				$this->session->set_flashdata('notice', 'Please enter the proper lecture data !');
			}
			
			redirect(base_url()."semester/timetable");
		}
		
		public function tests()
		{
			$user_id = $this->session->userdata('user_id');
			$sem =$this->sem_model->get_sem($user_id);
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
						
			$data['title'] = 'Test Shedule';
			$data['sem_id'] = $sem;
			$data['user_id'] = $user_id;
			$data['subjects_data'] = $this->sem_model->get_subjects($sem);
			$data['test_cat'] = $this->sem_model->get_test_cat($sem);
			
			$this->load->view('templates/header', $data);
			$this->load->view('semester/exam_sch_view', $data);
			$this->load->view('templates/footer');
		}

		public function add_test($sem_id)
		{
			$this->sem_model->add_exam($sem_id);
			$this->session->set_flashdata('notice','Test has been added successfully !');
			redirect(base_url()."semester/tests");	
		}
		
		public function delete_test($test_id)
		{
			$this->sem_model->delete_test($test_id);
			$this->session->set_flashdata('notice', 'test has been deleted successfully !');
			redirect(base_url()."semester/tests");
			
		}
		
		public function notice()
		{
			$user_id = $this->session->userdata('user_id');
			$sem =$this->sem_model->get_sem($user_id);
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			
			$data['title'] = 'Notifications';
			$data['sem_id'] = $sem;
			$data['user_id'] = $user_id;
			$data['notices_data'] = $this->sem_model->get_notice($sem);
			
			$this->load->view('templates/header', $data);
			$this->load->view('semester/notice_view', $data);
			$this->load->view('templates/footer');
		}

		public function add_notice($sem_id)
		{
			$user_id = $this->session->userdata('user_id');
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			if(isset($_POST['submit']))
			{
				$this->sem_model->set_notice($sem_id);
				$this->session->set_flashdata('notice', 'the notice has been added successfully !');
				redirect(base_url()."semester/notice");
			}
						
			$data['title'] = 'Add Notice';
			$data['sem_id'] = $sem_id;
			$data['user_id'] = $user_id;
			
			$this->load->view('templates/header', $data);
			$this->load->view('semester/add_notice_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function delete_notice($nid)
		{
			$this->sem_model->delete_notice($nid);
			$this->session->set_flashdata('notice', 'the notice has been deleted successfully !');
			redirect(base_url().'semester/notice');
		}

		public function subject($sub_id)
		{
			$user_id = $this->session->userdata('user_id');
			$sem =$this->sem_model->get_sem($user_id);
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			
			$data['title'] = $this->sem_model->get_sub_name($sub_id);
			$data['sem_id'] = $sem;
			$data['user_id'] = $user_id;
			$data['sub_id'] = $sub_id;
			$data['subject_data'] = $this->sem_model->get_subjectdata($sub_id);
			$data['exps_data'] = $this->sem_model->get_experiments($sub_id);
			$data['asgns_data'] = $this->sem_model->get_assignments($sub_id);
			$data['books_data'] = $this->sem_model->get_booksdata($sub_id);
						
			$this->load->view('templates/header', $data);
			$this->load->view('semester/subject_view', $data);
			$this->load->view('templates/footer');	
		}
		
		public function updt_subject($sub_id)
		{
			$user_id = $this->session->userdata('user_id');
			$sem =$this->sem_model->get_sem($user_id);
			
			$data['title'] = $this->sem_model->get_sub_name($sub_id);
			$data['sem_id'] = $sem;
			$data['user_id'] = $user_id;
			$data['sub_id'] = $sub_id;
			
			if (isset($_POST['submit'])) {
				if($this->input->post('category') == 'assignments and tutorials'){	
					$assgn_title = $this->input->post('title');
					$assgn_des = $this->input->post('description');
					$submission_date = $this->input->post('submission_date');
					$assgn_add_date = date("Y-m-d");
					$this->sem_model->add_assignment($sub_id, $assgn_title, $assgn_des, $submission_date, $assgn_add_date);
					$this->session->set_flashdata('notice', 'assignment has been added successfully !');
					redirect(base_url()."semester/subject/".$sub_id);
					
				}elseif($this->input->post('category') == 'experiments'){
						
					$exp_title = $this->input->post('title');
					$exp_des = $this->input->post('description');
					$this->sem_model->add_experiment($sub_id, $exp_title, $exp_des);
					$this->session->set_flashdata('notice', 'experiment has been added successfully !');
					redirect(base_url()."semester/subject/".$sub_id);
					
				}else{
					
					$description = $this->input->post('description');
					$this->sem_model->updt_description($sub_id, $description);
					$this->session->set_flashdata('notice', 'description has been updated successfully !');
					redirect(base_url()."semester/subject/".$sub_id);
				}
			}

			if (isset($_POST['bsubmit'])) {
				$this->sem_model->add_refbook($sub_id);
				$this->session->set_flashdata('notice', 'reference book has been added successfully !');
				redirect(base_url()."semester/subject/".$sub_id);
			}
			
						
			$this->load->view('templates/header', $data);
			$this->load->view('semester/add_semdata_view', $data);
			$this->load->view('templates/footer');				
		}
		
		public function delete_enteries($sub_id)
		{
			$user_id = $this->session->userdata('user_id');
			$sem =$this->sem_model->get_sem($user_id);
			
			$data['title'] = $this->sem_model->get_sub_name($sub_id);
			$data['sem_id'] = $sem;
			$data['user_id'] = $user_id;
			$data['sub_id'] = $sub_id;
			$data['subject_data'] = $this->sem_model->get_subjectdata($sub_id);
			$data['exps_data'] = $this->sem_model->get_experiments($sub_id);
			$data['asgns_data'] = $this->sem_model->get_assignments($sub_id);
			$data['books_data'] = $this->sem_model->get_booksdata($sub_id);
						
			$this->load->view('templates/header', $data);
			$this->load->view('semester/delete_enteries_view', $data);
			$this->load->view('templates/footer');				
		}
		
		public function delete_experiment($sub_id,$exp_id)
		{
			$this->sem_model->delete_experiment($exp_id);
			$this->session->set_flashdata('notice', 'the experiment has been successfully deleted !');
			redirect(base_url().'semester/delete_enteries/'.$sub_id);
		}		
		
		public function delete_assignment($sub_id, $assgn_id)
		{
			$this->sem_model->delete_assignment($assgn_id);
			$this->session->set_flashdata('notice', 'the assignment has been successfully deleted !');
			redirect(base_url().'semester/delete_enteries/'.$sub_id);
		}

		public function delete_refbook($sub_id, $book_id)
		{
			$this->sem_model->delete_refbook($book_id);
			$this->session->set_flashdata('notice', 'the book has been successfully deleted !');
			redirect(base_url().'semester/delete_enteries/'.$sub_id);
		}
		
		public function files($sem_id)
		{
			$user_id = $this->session->userdata('user_id');
			$sem =$this->sem_model->get_sem($user_id);
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			
			$data['title'] = 'Files';
			$data['sem_id'] = $sem;
			$data['user_id'] = $user_id;
			$data['subject_data'] = $this->sem_model->get_subjects($sem_id);
						
			$this->load->view('templates/header', $data);
			$this->load->view('semester/add_file_view', $data);
			$this->load->view('templates/footer');		
		}
		
		public function upload_file($sem_id)
		{
			//$config['upload_path'] = './files'; /* NB! create this dir! */
      			//$config['allowed_types'] = 'zip';
			//$config['max_size']	= '30000';
      		
      			//$length = 10;
			//$characters = '123456789abcdefghijklmnopqrstuvwxyz';
			//$string = '';    
			//for ($p = 0; $p < $length; $p++) 
			//{
			//	$string .= $characters[mt_rand(0, strlen($characters)-1)];
			//}
      		
      			//this section has to be modified to avoid duplicate enteries
      			//$config['file_name'] = $string;
			
			
      			/* Load the upload library */
      			//$this->load->library('upload', $config);



      			/*if (!$this->upload->do_upload('file'))
			{
				$data['title'] = 'Files';
				$data['error'] = array('error' => $this->upload->display_errors());
				$data['ref_url'] = base_url().'semester/files/'.$sem_id;
				$this->load->view('templates/header', $data);
				$this->load->view('pages/upload_error_view', $data);
				$this->load->view('templates/footer');
			}*/
			//else
			//{
				//$data = $this->upload->data();
				
				//$file_name = $data['file_name'];
				$user_id = $this->session->userdata('user_id');
				
				$this->sem_model->set_file($user_id, $sem_id);
				$title_id = $this->update_model->update($user_id, "fshare", "fid");
				$this->session->set_flashdata('notice', 'the file has been successfully added !');
				
				//-------------------------------------------------------------------
				//sending email for update
				
				$userdata = $this->login_model->get_user_info($user_id);
				$username = $userdata['username'];
				$profile_pic = $userdata['thumb_profile_pic'];
				$path = PP_UPLOADPATH.$userdata['thumb_profile_pic'];
			
				$title = $this->input->post('title');
				$subject = 'File Upload Update';
				$category = $this->input->post('category');
				$url_category = str_replace(" ", "-", $category);
			
				$url = "http://www.compag.in/semester/view_files/$url_category/$sem_id";
				$description = $this->input->post('description');
				$message = '<table ><tr>
				<td style = width:60px;><image src = "'.$path.'" width = "55" height = "55" alt = "profile pic"></td>
				<td><a href = '.base_url().'profile/view/'.$user_id.'>'.$username.'</a> added a file in '.$category.'<br /><br />Download it <b><a class = a href = '.$url.' target = _blank>here</a>
				</b><br /><p style = "text-align:justify;font-weight: bold;color:#CCCCCC;">'.$description.'</p></td>
				</tr></table>';
			
				$emails = $this->login_model->get_email_ids($sem_id);
				
				update_email($message, $subject, $emails);
				
				
				//-------------------------------------------------------------------
				
				redirect(base_url().'semester/view_files/'.str_replace(" ", "-", $this->input->post('category')).'/'.$sem_id);	
			//}
			
		}

		public function view_files($category, $sem_id)
		{
			$user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'Files';
			$data['sem_id'] = $sem_id;
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
		
			$category = str_replace("-", " ",$category);
			$data['category'] = $category;
			$data['files_data'] = $this->sem_model->get_files($category);
						
			$this->load->view('templates/header', $data);
			$this->load->view('semester/view_files_view', $data);
			$this->load->view('templates/footer');
		}

		public function download($filename)
		{

			// define the path to the download folder
			$getfile = NULL;
			// block any attempt to explore the filesystem
			if ($filename)
			{
    			$getfile = $filename;
			} else
			{
    			exit;
			}
			if ($getfile)
			{
    	
	    			$path = $_SERVER['DOCUMENT_ROOT']."/files/$getfile";
					
				//chmod($_SERVER['DOCUMENT_ROOT']."/compag.in/profile_pics/$profile_image_name" , 0777);
	    			// check that it exists and is readable
	    			if (file_exists($path) && is_readable($path)) 
	    			{
	       				
	
						header('Content-Type: application/octet-stream');
						header("Content-Disposition: attachment; filename=$filename");
						ob_clean();
						flush();
						readfile($path);
	           		 		force_download($filename, "this is a test!");
				}
	        		else 
	        		{
	        			$data['title'] = 'error'; 
	           			$this->load->view('templates/header', $data);
					$this->load->view('pages/error_view');
					$this->load->view('templates/footer');
			       }
			}
		}
	}

	
?>