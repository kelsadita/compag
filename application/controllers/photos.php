<?php
	
	/**
	 * 
	 */
	class Photos extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->model('photos_model');
			$this->load->model('login_model');
			$this->load->model('update_model');
			$this->load->model('profile_model');
			$this->load->library('session');
			$this->load->helper('form');
			$this->load->helper('url');
			
		}
		
		public function index()
		{
			$user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'Photos';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$data['albums_info'] = $this->photos_model->get_albums_list();
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/photos/photos_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function view($album_id)
		{
			$user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'Photos';
			$data['album_info'] = $this->photos_model->get_album_info($album_id);
			$data['photos_info'] = $this->photos_model->get_photo_info($album_id);
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/photos/view_album_view', $data);
			$this->load->view('templates/footer');	
		}
		
		public function add_album()
		{
			$user_id = $this->session->userdata('user_id');
			
			$this->photos_model->set_album($user_id);
			$this->update_model->update($user_id, "albums", "album_id");
						
			$album_id = $this->photos_model->get_album_id($user_id);
			redirect('photos/view/'.$album_id);
		}
		
		public function upload_photo($album_id)
		{
			if(isset($_POST['upload'])) {
      		/* Create the config for upload library */
      		/* (pretty self-explanatory) */
      		$config['upload_path'] = './album_photos'; /* NB! create this dir! */
      		$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
      		$config['max_size']  = '2097152';
      		
      		/* Load the upload library */
      		$this->load->library('upload', $config);

      		/* Create the config for image library */
      		/* (pretty self-explanatory) */
      		$configThumb = array();
      		$configThumb['image_library'] = 'gd2';
      		$configThumb['source_image'] = '';
      		$configThumb['create_thumb'] = TRUE;
      		$configThumb['maintain_ratio'] = TRUE;
      		/* Set the height and width or thumbs */
      		/* Do not worry - CI is pretty smart in resizing */
      		/* It will create the largest thumb that can fit in those dimensions */
      		/* Thumbs will be saved in same upload dir but with a _thumb suffix */
      		/* e.g. 'image.jpg' thumb would be called 'image_thumb.jpg' */
      		$configThumb['width'] = 300;
      		$configThumb['height'] = 400;
      		/* Load the image library */
      		$this->load->library('image_lib');

      		if (!$this->upload->do_upload('image'))
			{
				$data['title'] = 'Photos';
				$data['error'] = array('error' => $this->upload->display_errors());
				$data['ref_url'] = base_url().'photos/view/'.$album_id;
				$this->load->view('templates/header', $data);
				$this->load->view('pages/upload_error_view', $data);
				$this->load->view('templates/footer');
			}
			else
			{
				$data = $this->upload->data();
				if($data['is_image'] == 1) 
				{
          			$configThumb['source_image'] = $data['full_path'];
          			$this->image_lib->initialize($configThumb);
          			$this->image_lib->resize();
        		}
				
				$thumb_name = $data['raw_name']."_thumb".$data['file_ext'];
				$image_name = $data['file_name'];
				$user_id = $this->session->userdata('user_id');
				
				$this->photos_model->set_photo($user_id,$album_id,$image_name,$thumb_name);
				
				redirect('photos/view/'.$album_id);	
			}

    		}

			
   		}

		public function edit($album_id, $photo_id)
		{
			$data['title'] = 'Photos';
			$data['photo_data'] = $this->photos_model->get_photo_data($photo_id);
			
			if(isset($_POST['save_changes']))
			{
				$this->photos_model->update_photo_data($photo_id);
				$this->session->set_flashdata('success','the photo has been updated successfully !');
				redirect('photos/view/'.$album_id);
			}
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/photos/photos_edit_view', $data);
			$this->load->view('templates/footer');
					
		}
		
		public function edit_album($album_id, $user_id)
		{
			if(isset($_POST['submit']))
			{
				$this->photos_model->update_album_data($album_id, $user_id);
				$this->session->set_flashdata('notice','the album has been updated successfully !');
				redirect('profile/albums/'.$user_id);
			}
			
		}
		
		public function delete_photo($photo_id, $album_id)
		{
			$data['photo_info'] = $this->photos_model->get_photo_data($photo_id);
			$image_name = $data['photo_info']['image_name'];
			chmod($_SERVER['DOCUMENT_ROOT']."/album_photos/$image_name" , 0777);
			@unlink($_SERVER['DOCUMENT_ROOT']."/album_photos/$image_name");
			
			$thumb_image_name = $data['photo_info']['thumb_name'];
			chmod($_SERVER['DOCUMENT_ROOT']."/album_photos/$thumb_image_name", 0777);
			@unlink($_SERVER['DOCUMENT_ROOT']."/album_photos/$thumb_image_name");
			$this->photos_model->delete_photo($photo_id);
			$this->session->set_flashdata('success', 'Photo has been deleted successfully !');
			redirect(base_url()."photos/view/".$album_id);	
		}
	}
	

?>