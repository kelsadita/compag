<?php

	/**
	 * Code Igniter controller for codes
	 */
	class Codes extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->model('codes_model');
			$this->load->model('login_model');
			$this->load->model('update_model');
			$this->load->model('profile_model');
			$this->load->library('email');
			$this->load->library('session');
			$this->load->helper('form');
			$this->load->helper('url');
		}
		
		public function index()
		{
			$user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'Codes';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$data['codes_categories'] = $this->codes_model->get_codes_categories();
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/codes/codes_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function submit_code()
		{
			$user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'Codes';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/codes/submit_code_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function category($category)
		{
			$category = str_replace("-", " ", $category);
			
			$data['title'] = 'Codes';
			$data['category'] = $category;
			$data['code_list'] = $this->codes_model->get_code_list($category);
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/codes/codes_category_view', $data);
			$this->load->view('templates/footer');
						
		}
		
		public function view($code_id)
		{
			$data['title'] = 'Codes';
			$data['code_info'] = $this->codes_model->get_code($code_id);
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/codes/view_code_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function add_code()
		{
			$user_id = $this->session->userdata('user_id');
			
			$this->codes_model->set_code($user_id);
			$title_id = $this->update_model->update($user_id, "codes", "code_id");
			
			//------------------------------------------------------------------//
			//email functionality....

			$userdata = $this->login_model->get_user_info($user_id);
			$username = $userdata['username'];
			$profile_pic = $userdata['thumb_profile_pic'];
			$path = PP_UPLOADPATH.$userdata['thumb_profile_pic'];
			
			$title = $this->input->post('code_title');
			$subject = 'Codes Update';
			$category = $this->input->post('code_category');
			$url = base_url()."codes/view/".$title_id;
			$message = '<table ><tr>
			<td style = width:60px;><image src = "'.$path.'" width = 55 height = 55></td>
			<td><a href = '.base_url().'profile/view/'.$user_id.'>'.$username.'</a> added a code for <b><a class = a href = '.$url.' target = _blank>'.$title.'</a>
			</b> in '.$category.'<br /></tr></table>';
			
			$emails = $this->login_model->get_email_ids();
				
			foreach($emails as $emaildata)
			{
				$this->email->from('admin@compag.in', 'Compag');
							
				$this->email->subject($subject);
				
				$this->email->to($emaildata['email']);
				$this->email->message($message);	
				$this->email->send();
			}
			//------------------------------------------------------------------//
			
			
			
			$this->session->set_flashdata('success', 'The code has been added successfully !');
			
			redirect('codes');
		}
		
		
	}
	
?>