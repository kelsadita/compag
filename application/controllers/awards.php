<?php

	/**
	 * 
	 */
	class Awards extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->model('awards_model');
			$this->load->model('login_model');
			$this->load->model('profile_model');
			$this->load->library('session');
			$this->load->helper('form');
			$this->load->helper('url');
		}
		
		public function index()
		{
			$user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'Awards';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$data['awards_info'] = $this->awards_model->get_awards_names();
			$data['jodis_info'] = $this->awards_model->get_jodis_info();
			$data['dostanas_info'] = $this->awards_model->get_dostanas_info();
			$data['users_info'] = $this->awards_model->get_users_info();
			$data['user_id'] = $user_id;
			$data['voted'] = $this->profile_model->is_voted($user_id);
			$data['from_comp'] = $this->awards_model->from_comp($user_id);
			$this->load->view('templates/header', $data);
			$this->load->view('pages/awards_view', $data);
			$this->load->view('templates/footer');
			
		}
		
		public function add_vote()
		{
			$user_id = $this->session->userdata('user_id');
			$user_id = $this->session->userdata('user_id');
			$this->awards_model->add_votes();
			$this->profile_model->set_voted($user_id);
			
			redirect(base_url()."awards");
		}
		
		public function admin()
		{
			$user_id = $this->session->userdata('user_id');
			$user_status = $this->session->userdata('status');
			
			$data['title'] = 'Awards';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$data['awards_info'] = $this->awards_model->get_awards_names();
			$data['jodis_info'] = $this->awards_model->get_jodis_info();
			$data['dostanas_info'] = $this->awards_model->get_dostanas_info();
			$data['users_info'] = $this->awards_model->get_users_info();
			$data['user_id'] = $user_id;
			$data['user_status'] = $user_status;
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('pages/awards_admin_view', $data);
			$this->load->view('templates/footer');
		}
		
	}
	
?>