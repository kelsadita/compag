<?php
	
	/**
	 * 
	 */
	class Profile extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->model('login_model');
			$this->load->model('profile_model');
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->helper('file');
		}
		
		public function view($user_id)
		{
			$data['profile_data'] = $this->profile_model->get_profile_info($user_id);
			$data['title'] = $data['profile_data']['username'];
			$data['user_id'] = $user_id;
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			
			$this->load->view('templates/header',$data);
			$this->load->view('profile/view_profile', $data);
			$this->load->view('templates/footer');
		}
		
		public function edit($user_id)
		{
			$data['profile_data'] = $this->profile_model->get_profile_info($user_id);
			$data['title'] = 'Edit Profile';
			$data['user_id'] = $user_id;
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			
			$this->load->view('templates/header',$data);
			$this->load->view('profile/edit_profile_view', $data);
			$this->load->view('templates/footer');
			
		}
		
		public function update($user_id)
		{
			$this->profile_model->update_profile($user_id);
			$this->session->set_flashdata('notice', 'Your profile has been updated sucessfully <a href='.base_url().'profile/view/'.$user_id.'>View Profile</a>!');
			redirect(base_url().'profile/edit/'.$user_id);
		}
		
		public function update_pic($user_id)
		{
			
      		$config['upload_path'] = './profile_pics'; 
      		$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
      		$config['max_size']  = '2097152';
			$config['file_name'] = $user_id;
      		
      		$this->load->library('upload', $config);

      		
      		$configThumb = array();
      		$configThumb['image_library'] = 'gd2';
      		$configThumb['source_image'] = '';
      		$configThumb['create_thumb'] = TRUE;
      		$configThumb['maintain_ratio'] = TRUE;
      		
      		$configThumb['width'] = 250;
      		$configThumb['height'] = 350;
      		
      		$this->load->library('image_lib');

      		if (!$this->upload->do_upload('profile_pic'))
			{
				$data['title'] = 'Profile pic';
				$data['error'] = array('error' => $this->upload->display_errors());
				$data['ref_url'] = base_url().'profile/view/'.$user_id;
				$this->load->view('templates/header', $data);
				$this->load->view('pages/upload_error_view', $data);
				$this->load->view('templates/footer');
			}
			else
			{	
				$data = $this->upload->data();
				if($data['is_image'] == 1) 
				{
          			$configThumb['source_image'] = $data['full_path'];
          			$this->image_lib->initialize($configThumb);
          			$this->image_lib->resize();
        		}
				
				$thumb_name = $data['raw_name']."_thumb".$data['file_ext'];
				$image_name = $data['file_name'];
				$user_id = $this->session->userdata('user_id');
				
				//-----------------------------------------------------------------------------------------------------
				$photo_data['photo_info'] = $this->login_model->get_user_info($user_id); 
				$profile_image_name = $photo_data['photo_info']['profile_pic'];
				
				if($profile_image_name != 'nopic.jpg')
				{
					$this->session->set_flashdata('notice', $profile_image_name);
					chmod($_SERVER['DOCUMENT_ROOT']."/profile_pics/$profile_image_name" , 0777);
					@unlink($_SERVER['DOCUMENT_ROOT']."/profile_pics/$profile_image_name");
				
					$thumb_image_name = $photo_data['photo_info']['thumb_profile_pic'];
					@unlink($_SERVER['DOCUMENT_ROOT']."/profile_pics/$thumb_image_name");
				}
				//-----------------------------------------------------------------------------------------------------	
				
				
			
				$this->profile_model->set_profilepic($user_id,$image_name,$thumb_name);
				
				$this->session->set_userdata('thumb_profile_pic', $thumb_name);
				
				redirect(base_url().'profile/view/'.$user_id);	
			}

    	
		}

		
		public function members()
		{
						
			$data['title'] = 'Members';
			$data['members_data'] = $this->profile_model->get_members_info();
			
			$this->load->view('templates/header', $data);
			$this->load->view('profile/members_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function albums($user_id)
		{
			$this->load->model('photos_model');
			
			$session_user_id = $this->session->userdata('user_id');
			$album_user_id = $user_id;
			
			$data['title'] = 'User Albums';
			$data['user_album_info'] = $this->photos_model->get_albums_list($user_id); 
			$data['user_id'] = $session_user_id;
			$data['album_user_id'] = $user_id;
			
			$this->load->view('templates/header', $data);
			$this->load->view('profile/user_album_view', $data);
			$this->load->view('templates/footer');
		}

		public function edit_album($album_id)
		{
			$this->load->model('photos_model');
			$data['title'] = 'Edit Album';
			$data['album_info'] = $this->photos_model->get_album_info($album_id);
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/photos/edit_album_view');
			$this->load->view('templates/footer');
		}
		
		public function delete_album($album_id, $user_id)
		{
			$this->load->model('photos_model');
			$this->photos_model->delete_album($album_id);
			$this->session->set_flashdata('notice', 'the album has been deleted successfully !');
			redirect(base_url()."profile/albums/".$user_id);
		}
		
		public function videos($user_id)
		{
			$this->load->model('videos_model');
			
			$session_user_id = $this->session->userdata('user_id');
			$video_user_id = $user_id;
			
			$data['title'] = 'User Videos';
			$data['user_videos_info'] = $this->videos_model->get_videos_info($user_id); 
			$data['user_id'] = $session_user_id;
			$data['video_user_id'] = $user_id;
			
			$this->load->view('templates/header', $data);
			$this->load->view('profile/user_videos_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function delete_video($video_id, $user_id)
		{
			$this->load->model('videos_model');
			$this->videos_model->delete_video($video_id);
			$this->session->set_flashdata('notice', 'the video has been deleted successfully !');
			redirect(base_url()."profile/videos/".$user_id);
		}
		
		public function codes($user_id)
		{
			$this->load->model('codes_model');
			
			$session_user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'User Codes';
			$data['categories'] = $this->codes_model->get_codes_categories($user_id);
			$data['user_id'] = $session_user_id;
			$data['codes_user_id'] = $user_id;
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('profile/user_codes_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function delete_code($user_id)
		{
			$this->load->model('codes_model');
			$code_id = $this->input->post('code_id');
			$this->codes_model->delete_code($user_id, $code_id);
		}
		
		public function discussions($user_id)
		{
			$this->load->model('discuss_model');
			
			$session_user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'User Topics';
			$data['user_id'] = $session_user_id;
			$data['topic_user_id'] = $user_id;
			$data['topics_info'] = $this->discuss_model->get_discussions(NULL, 0, 0, $user_id);
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('profile/user_topics_view', $data);
			$this->load->view('templates/footer');
		}

		public function delete_topic($user_id)
		{
			$this->load->model('discuss_model');
			$did = $this->input->post('did');
			$this->discuss_model->delete_topic($user_id, $did);
		}

		public function links($user_id)
		{
			$this->load->model('links_model');
			
			$session_user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'User Links';
			$data['categories'] = $this->links_model->get_link_categories($user_id);
			$data['user_id'] = $session_user_id;
			$data['links_user_id'] = $user_id;
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('profile/user_links_view', $data);
			$this->load->view('templates/footer');
		}

		public function delete_link($user_id)
		{
			$this->load->model('links_model');
			$link_id = $this->input->post('link_id');
			$this->links_model->delete_link($user_id, $link_id);	
		}
	}
	

?>