<?php
	
	/**
	 * 
	 */
	class Links extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->model('links_model');
			$this->load->model('login_model');
			$this->load->model('update_model');
			$this->load->library('session');
			$this->load->library('email');
			$this->load->helper('form');
			$this->load->helper('url');
		}
		
		public function index()
		{
			$user_id = $this->session->userdata('user_id');
				
			$data['title'] = 'Links';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$data['links_categories'] = $this->links_model->get_link_categories();
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/links/links_view', $data);
			$this->load->view('templates/footer');

		}
		
		public function category($category)
		{
			$data['title'] = $category;	
			$category = str_replace("-", " ", $category);
			$data['cat_contents'] = $this->links_model->get_cat_content($category);
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/links/links_category_view', $data);
			$this->load->view('templates/footer');
		}
		
		
		public function add_link()
		{
			$user_id = $this->session->userdata('user_id');
			
			$this->links_model->set_link($user_id);
			$title_id = $this->update_model->update($user_id, "link", "link_id");
					
			
			//------------------------------------------------------------------//
			//email functionality....

			$userdata = $this->login_model->get_user_info($user_id);
			$username = $userdata['username'];
			$profile_pic = $userdata['thumb_profile_pic'];
			$path = PP_UPLOADPATH.$userdata['thumb_profile_pic'];
			
			$title = $this->input->post('title');
			$subject = 'Link Update';
			$category = $this->input->post('category');
			$url = $this->input->post('url');
			$description = $this->input->post('description');
			$message = '<table ><tr>
			<td style = width:60px;><image src = "'.$path.'" width = 55 height = 55></td>
			<td><a href = '.base_url().'profile/view/'.$user_id.'>'.$username.'</a> added a link for <b><a class = a href = '.$url.' target = _blank>'.$title.'</a>
			</b> in '.$category.'<br /><p style = "text-align:justify;font-weight: bold;color:#CCCCCC;">'.$description.'</p></td>
			</tr></table>';
			
			$emails = $this->login_model->get_email_ids();
				
			foreach($emails as $emaildata)
			{
				$this->email->from('admin@compag.in', 'Compag');
							
				$this->email->subject($subject);
				
				$this->email->to($emaildata['email']);
				$this->email->message($message);	
				$this->email->send();
			}
			//------------------------------------------------------------------//
			
			$this->session->set_flashdata('success','The link has been added successfully!');
			redirect('links');
		}
	}
	
?>