<?php
	
	/**
	 * 
	 */
	class Discuss extends CI_Controller {
		
		public function __construct() {
			
			parent::__construct();
			$this->load->model('discuss_model');
			$this->load->model('login_model');
			$this->load->model('update_model');
			$this->load->model('profile_model');
			$this->load->library('session');
			$this->load->helper('form');
			$this->load->helper('url');
			
		}
		
		public function most_recent($offset = 0)
		{
			$ref = 'most-recent';
			$user_id = $this->session->userdata('user_id');
			$data['title'] = 'Discuss';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$data['ref']= $ref;
			
			
			//scripting for pagination
			
			$limit = 8;
			$data['discussions_data'] = $this->discuss_model->get_discussions($ref, $limit, $offset);
			$data['count'] = $this->discuss_model->countrows();
			
			$this->load->library('pagination');
			$config = array(
				
				'base_url' => base_url()."discuss/most_recent",
				'total_rows' => $data['count'],
				'per_page' => $limit,
				'num_links' => 2
			);
			
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links(); 
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/discuss/discuss_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function most_discussed($offset= 0)
		{
			$ref = 'most-discussed';
			$user_id = $this->session->userdata('user_id');
			$data['title'] = 'Discuss';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$data['ref'] = $ref; 
			
			//scripting for pagination
			
			$limit = 8;
			$data['discussions_data'] = $this->discuss_model->get_discussions($ref, $limit, $offset);
			$data['count'] = $this->discuss_model->countrows();
			
			$this->load->library('pagination');
			$config = array(
				
				'base_url' => base_url()."discuss/most_discussed",
				'total_rows' => $data['count'],
				'per_page' => $limit,
				'num_links' => 2
			);
			
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links(); 
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/discuss/discuss_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function add()
		{
			$user_id = $this->session->userdata('user_id');
			
			if(isset($_POST['submit']))
			{
				$description = strip_tags($this->input->post('description'));
				if(empty($description))
				{
					$this->session->set_flashdata('notice','Please describe your topic !');
					redirect('discuss/add');
				}
				$this->discuss_model->set_topic($user_id);
				$this->update_model->update($user_id, "discuss","did");
				$this->session->set_flashdata('notice','The topic has been added successfully !');
				redirect('discuss/most_recent');
			}
			else 
			{
				$data['title'] = 'Discuss';
				$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			
				$this->load->view('templates/header', $data);
				$this->load->view('activities/discuss/add_topic_view', $data);
				$this->load->view('templates/footer');
			
			}
			
		}
		
		public function view($did)
		{
			$user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'Discuss'; 
			$data['topic_info'] = $this->discuss_model->get_topic($did);
			$data['answers_data'] = $this->discuss_model->get_ans_data($did);
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			
			$this->load->view('templates/header', $data);
			$this->load->view('activities/discuss/view_topic_view', $data);
			$this->load->view('templates/footer');
		}

		public function addans($did)
		{
			$user_id = $this->session->userdata('user_id');
			
			$this->discuss_model->set_ans($user_id, $did);
			redirect(base_url().'discuss/view/'.$did);
		}

		public function ans_response($user_id, $ans_id, $did)
		{
			$this->discuss_model->update_likes($user_id, $ans_id, $did);
			$data['response'] = $this->discuss_model->get_num_likes($ans_id);
			$this->load->view('activities/discuss/ans_response_view', $data);			
		}
		
	}
	
	

?>