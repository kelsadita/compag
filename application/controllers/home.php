<?php
	
	/**
	 * 
	 */
	class Home extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->library('session');
			$this->load->model('login_model');
			$this->load->model('home_model');
			$this->load->model('links_model');
			$this->load->model('discuss_model');
			$this->load->model('codes_model');
			$this->load->model('photos_model');
			$this->load->model('videos_model');
			$this->load->model('sem_model');
			$this->load->helper('url');
		}
		
		public function index($offset = 0)
		{
			$data['title'] = 'Home';
			$user_id = $this->session->userdata('user_id');
			
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			
			//pagination scripting
			$limit = 8;
			$data['feeds_data'] = $this->home_model->get_feeds($limit, $offset);
			$data['count'] = $this->home_model->countrows();
			
			$this->load->library('pagination');
			$config = array(
				
				'base_url' => base_url()."home/index",
				'total_rows' => $data['count'],
				'per_page' => $limit,
				'num_links' => 2
			);
			
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links(); 
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('pages/home_view', $data);
			$this->load->view('templates/footer');

		}
		
	}
	
?>
