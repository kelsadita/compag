<?php

	/**
	 * 
	 */
	class Pings extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->model('ping_model');
			$this->load->model('login_model');
			$this->load->model('profile_model');
			$this->load->library('session');
			$this->load->helper('form');
			$this->load->helper('url');
		}
		
		public function index()
		{
			$user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'Pings';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$data['pings_data'] = $this->ping_model->get_pings($user_id);
			$data['user_id'] = $user_id;
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('ping/pings_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function view($from_id)
		{
			$user_id = $this->session->userdata('user_id');
			$this->ping_model->update_notice($from_id, $user_id);
			
			$data['title'] = 'Pings';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$data['msgs_data'] = $this->ping_model->get_from_user_pings($from_id, $user_id);
			$data['user_id'] = $user_id;
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('ping/view_pings_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function write()
		{
			$user_id = $this->session->userdata('user_id');
			
			$data['title'] = 'Pings';
			$data['msg_count'] = $this->login_model->get_msg_count($user_id);
			$data['usernames'] = $this->login_model->get_usernames();
			$data['user_id'] = $user_id;
			
			
			$this->load->view('templates/header', $data);
			$this->load->view('ping/write_ping_view', $data);
			$this->load->view('templates/footer');
		}
		
		public function send_msg()
		{
			$sent = $this->ping_model->send_msg();
			if($sent == 1)
			{
				$this->session->set_flashdata('notice', 'Your message has been sent successfully !');
			}
			else {
				$this->session->set_flashdata('notice', 'No such user exist !');
			}
			
			redirect(base_url()."pings/write");
		}
	}
	
?>