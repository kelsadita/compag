<?php

	/**
	 * 
	 */
	class Discuss_model extends CI_Model {
		
		public function __construct() {
			$this->load->database();
		}
		
		public function get_discussions($ref = NULL, $limit = 8, $offset = 0, $user_id = 0)
		{
			if($ref == 'most-recent')
			{
				$query = $this->db->select('*')
						->from('discuss')
						->order_by('add_date_time', 'desc')
						->limit($limit, $offset);
			}
			elseif ($ref == NULL) 
			{
				$query = $this->db->select('*')
						->from('discuss')
						->where('user_id', $user_id);		
			}
			else 
			{
				$query = $this->db->select('*')
						->from('discuss')
						->order_by('num_ans', 'desc')
						->limit($limit, $offset);
			}
			return $query->get()->result_array();
		}
		
		public function countrows()
		{
			$query = $this->db->get('discuss');
			return $query->num_rows();	
		}
		
		public function set_topic($user_id)
		{
			$now = date('Y-m-d, H:i:S');
			$data = array(
				
				'user_id' => $user_id,
				'topic' => $this->input->post('topic'),
				'description' => $this->input->post('description'),
				'add_date_time' => $now
			);
			
			$this->db->insert('discuss', $data);
			
		}
		
		public function get_topic($did)
		{
			$query = $this->db->get_where('discuss', array('did' => $did));
			return $query->row_array();
		}
		
		public function get_ans_data($did)
		{
			$query = $this->db->get_where('topic', array('did' => $did));
			return $query->result_array();
		}
		
		public function set_ans($user_id, $did)
		{
			$now = date('Y-m-d H:i:s');
			$data = array(
			
				'did' => $did, 
				'user_id' => $user_id,
				'ans' => $this->input->post('ans'),
				'ans_add_datetime' => $now
			);
			
			$this->db->insert('topic', $data);
			
			//updating the number of answers for the corresponding discussion.
			
			$query = $this->db->get_where('discuss', array('did' => $did));
			$row = $query->row_array();
			$num_ans = $row['num_ans'];
			
			$update_data = array(
			
				'num_ans' => $num_ans+1
			);
			
			$this->db->where('did', $did);
			$this->db->update('discuss', $update_data);
		}

		public function update_likes($user_id, $ans_id, $did)
		{
			$query = $this->db->get_where('ans_vote', array('ans_id' => $ans_id, 'user_id' => $user_id));
			if($query->num_rows() > 0)
			{
				$this->db->where('ans_id', $ans_id);
				$this->db->where('user_id', $user_id);
				$this->db->delete('ans_vote');
			}
			else
			{
				$data = array(
		
				'ans_id' => $ans_id,
				'user_id' => $user_id,
				'did' => $did
				);
			
				$this->db->insert('ans_vote', $data);
	
			}
			
		}
		
		public function get_num_likes($ans_id)
		{
			$query = $this->db->get_where('ans_vote', array('ans_id' => $ans_id));
			return $query->num_rows();
		}
		
		public function delete_topic($user_id, $did)
		{
			$this->db->where('did', $did);
			$this->db->where('user_id', $user_id);
			$this->db->delete('discuss');
			
			$this->db->where('did', $did);
			$this->db->delete('topic');
			
			$this->db->where('did', $did);
			$this->db->delete('ans_vote');
			
			$this->db->where('title_id', $did);
			$this->db->where('user_id', $user_id);
			$this->db->delete('recent_updates');	
		}
	}
	
?>