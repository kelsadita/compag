<?php
	
	/**
	 * 
	 */
	class Photos_model extends CI_Model {
		
		public function __construct() {
			$this->load->database();
		}
		
		public function get_albums_list($user_id = 0)
		{
			if ($user_id != 0) 
			{
				$this->db->order_by('album_add_datetime desc');
				$query = $this->db->get_where('albums', array('user_id' => $user_id));
			}
			else 
			{
				$this->db->order_by('album_add_datetime desc');
				$query = $this->db->get('albums');
			}
			return $query->result_array();
		}
		
		public function get_cover($album_id)
		{
			$this->db->select('thumb_name');
			$query = $this->db->get_where('photos', array('album_id' => $album_id));
			$row =  $query->row_array();
			
			if($query->num_rows() == 0)
			{
				return 'heaton.jpg';
			}
			else 
			{
				return $row['thumb_name'];
			}
		
		}
		
		public function get_album_info($album_id)
		{
			$query = $this->db->get_where('albums', array('album_id' => $album_id));
			return $query->row_array();
		}
		
		public function get_photo_info($album_id)
		{
			$query = $this->db->get_where('photos', array('album_id' => $album_id));
			return $query->result_array();
		}
		
		public function set_album($user_id)
		{
			$now = date('Y-m-d H:i:s');
			$data = array(
				
				'user_id' => $user_id,
				'name' => $this->input->post('album_name'),
				'description' => $this->input->post('description'),
				'album_add_datetime' => $now
			);
			
			$this->db->insert('albums', $data);
		}

		public function get_album_id($user_id)
		{
			$this->db->select_max('album_id');
			$query = $this->db->get_where('albums', array('user_id' => $user_id));
			$row = $query->row();
			
			return $row->album_id;
		}
		
		public function set_photo($user_id, $album_id, $image_name, $thumb_name)
		{
			$data = array(
				
				'user_id' => $user_id,
				'album_id' => $album_id,
				'image_name' => $image_name,
				'thumb_name' => $thumb_name
			);
			
			$this->db->insert('photos', $data);
		}
		
		public function get_photo_data($photo_id)
		{
			$query = $this->db->get_where('photos', array('photo_id' => $photo_id));
			return $query->row_array();
		}
		
		public function update_photo_data($photo_id)
		{
			$data = array(
			
				'pname' => $this->input->post('photo_name'),
				'pdescription' => $this->input->post('description')
			);
			
			$this->db->where('photo_id', $photo_id);
			$this->db->update('photos', $data);
		}
		
		public function update_album_data($album_id, $user_id)
		{
			$data = array(
			
				'name' => $this->input->post('album_name'),
				'description' => $this->input->post('description')
			
			);
			
			$this->db->where('album_id', $album_id);
			$this->db->where('user_id', $user_id);
			$this->db->update('albums', $data);
		}
		
		public function delete_photo($photo_id)
		{
		
			$this->db->where('photo_id', $photo_id);
			$this->db->delete('photos');
		}
		
		public function	delete_album($album_id)
		{
			$this->db->where('album_id', $album_id);
			$this->db->delete('albums');
			$this->db->where('album_id', $album_id);
			$this->db->delete('photos');
			$this->db->where('title_id', $album_id);
			$this->db->delete('recent_updates');
		}
	}
	
?>