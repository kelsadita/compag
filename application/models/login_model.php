<?php
	
	/**
	 * 
	 */
	class Login_model extends CI_Model {
		
		public function __construct() {
			
			$this->load->database();
		}
		
		public function add_user($username, $password1, $email, $gender, $birthdate)
		{
			$password = sha1($password1);
			$data = array(
				'username' => $username,
				'password' => $password,
				'email' => $email,
				'gender' => $gender,
				'birthdate' => $birthdate,
				'profile_pic' => 'nopic.jpg',
				'thumb_profile_pic' => 'nopic.jpg'
			);
			
			$this->db->insert('profile', $data);
			
		}
		
		public function add_confirmation($email, $key)
		{
			$data = array(
				'email' => $email,
				'confir_id' => $key
			);
			
			$this->db->insert('request', $data);	
		}
		
		public function chk_user($key)
		{
			$query = $this->db->get_where('request', array('confir_id'=> $key));
			if($query->num_rows() == 1)
			{
				$userdata = $query->row();
				$email = $userdata->email;
				
				//------------------------------------
				$this->db->where('email', $email);
				$data = array(
					'confirmed' => 1
				);
				$this->db->update('profile', $data);
				//------------------------------------
				
				$this->db->where('confir_id', $key);
				$this->db->delete('request');
				
				return 1;
			}
			else
			{
				return 0;
			}
		}
		
		public function chk_email($email)
		{
			$query = $this->db->get_where('profile', array('email'=> $email));
			return $query->num_rows();
		}
		
		public function isNotConfirmed($email, $password)
		{
			$password = sha1($password);
			$query = $this->db->get_where('profile',array('email' => $email, 'password' => $password, 'confirmed' => 0));
			return $query->num_rows();
		}
		/**Adding new function for mobile app login authentication 
		  */
		public function isAuth_for_mobile_app($email, $password) {
			$query = $this->db->get_where('profile',array('email' => $email, 'password' => $password, 'confirmed' => 1));
			return $query->num_rows();
		}
		public function isAuth($email, $password)
		{
			$password = sha1($password);
			$query = $this->db->get_where('profile',array('email' => $email, 'password' => $password, 'confirmed' => 1));
			return $query->num_rows();
		}
		
		public function get_info($email)
		{
			$query = $this->db->query("select user_id, username, status, profile_pic, thumb_profile_pic from profile where email='$email'");
			return $query->row_array();
		}
		
		public function user_count($email)
		{
			$query = $this->db->get_where('profile', array('email' => $email));
			return $query->num_rows();
		}
		
		public function get_msg_count($user_id)
		{
			$query = $this->db->get_where('message', array('sent_to' => $user_id, 'notice' => 0));
			return $query->num_rows();
		}
		
		public function get_user_info($user_id)
		{
			$query = $this->db->query("select username,profile_pic,thumb_profile_pic,status from profile where user_id = $user_id");
			return $query->row_array();
		}
		
		public function get_usernames()
		{
			$this->db->select('username');
			$query = $this->db->get('profile');
			return $query->result_array();	
		}
		
		public function set_resetpassdata($to, $confir_id)
		{
			$query = $this->db->get_where('rpassword', array('email' => $to));
			
			if($query->num_rows() == 1)
			{
				return 1;
			}
			else
			{	$data = array(
				'email' => $to,
				'confir_id' => $confir_id
				);
				$this->db->insert('rpassword', $data);

				return 0;
			}
						
		}
		
		public function reset_pass_data($key)
		{
			$query = $this->db->query("select * from rpassword where confir_id = '".$key."'");
			
			if($query->num_rows() == 0)
			{
				return 'nodata';
			}
			else
			{
				$user_data =  $query->row_array();
				$email = $user_data['email'];
				return $email;
			}
		}
		
		public function reset_password($password1, $email)
		{
			$this->db->where('email', $email);
			$password = sha1($password1);
			$data = array(
				'password' => $password
			);
			$this->db->update('profile', $data);
		}
		
		public function delete_key($key)
		{
			$this->db->where('confir_id', $key);
			$this->db->delete('rpassword');
			
		}
		
		//----------------------------followings are the functions of remote login----------------------------------------------//
		
		//adding key for the remote login
		public function add_key($pub_key, $private_key, $added_on)
		{
			$data = array(
				'pub_key' => $pub_key,
				'private_key' => $private_key,
				'added_on' => $added_on
			);
			
			$this->db->insert('remote_login', $data);
		}
		
		public function chk_collision($url_shrt_string){
			$query = $this->db->get_where('remote_login',array('shrt_id' => $url_shrt_string));
			return $query->num_rows();
		}
		
		public function set_urlShrtKey($pub_key, $url_shrt_string){
			$data = array(
				
				'shrt_id' => $url_shrt_string
			);
			$this->db->where('pub_key', $pub_key);
			$this->db->update('remote_login', $data);
		}
		
		//getting public key corresponding to shrt_id
		public function get_public_key($key){
			$query = $this->db->get_where('remote_login', array('shrt_id' => $key));
			return $query->row()->pub_key;
		}
		
		
		
		//Add the user id when the login is successfull.
		public function updt_romote_login($key, $user_id)
		{
			$this->db->where('pub_key', $key);
			$data = array('user_id' => $user_id);
			$this->db->update('remote_login', $data);
		}
		
		public function get_remote_userid($key)
		{
			
			$query = $this->db->get_where('remote_login', array('private_key' => $key));
			return $query->row()->user_id;
		}
		
		
		public function delete_remote_key($key)
		{
			$this->db->where('private_key', $key);
			
			$this->db->delete('remote_login');
		}
		
		public function verify($key)
		{
			$this->db->where('pub_key', $key);
			$data = array(
				'verify' => 1
			);
			$this->db->update('remote_login', $data);
		}
		
		public function is_privatekey_authenticated($key)
		{
			$Pquery = $this->db->get_where('remote_login',array('private_key' => $key));
			$present =  $Pquery->num_rows();
			$Vquery = $this->db->get_where('remote_login', array('private_key' => $key, 'verify' => 1));
			$verified = $Vquery->num_rows();
			
			if($present == 1 && $verified == 1)
			{
				return 1;
			}
		}
		
		public function is_publickey_authenticated($key)
		{
			$Pquery = $this->db->get_where('remote_login',array('pub_key' => $key));
			$present =  $Pquery->num_rows();
			$Vquery = $this->db->get_where('remote_login', array('pub_key' => $key, 'verify' => 1));
			$verified = $Vquery->num_rows();
			
			if($present == 1 && $verified == 1)
			{
				return 1;
			}
		}
		
		public function get_hash_password($email){
			
			$query = $this->db->get_where('profile', array('email' => $email));
			return $query->row_array();
		}
		
		//--------------------------------------------------------
		//getting the email ids for updates
		public function get_email_ids($sem_id = 0)
		{
			$this->db->select('email');
			if($sem_id == 0)
			{
				$query = $this->db->get('profile');
			}
			else
			{
				$query = $this->db->get_where('profile', array('sem' => $sem_id));
			}
			return $query->result_array();
		}
	}
	

?>