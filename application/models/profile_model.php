<?php
		
	/**
	 * 
	 */
	class Profile_model extends CI_Model {
		
		public function __construct() {
			$this->load->database();
		}
		
		public function get_profile_info($user_id)
		{
			$query = $this->db->get_where('profile', array('user_id' => $user_id));
			return $query->row_array();
		}
		
		public function get_members_info()
		{
			$query = $this->db->query("select user_id, username, profile_pic, thumb_profile_pic from profile order by username");
			return $query->result_array();
		}
		
		public function update_profile($user_id)
		{
			$data = array(
			
				'username' => $this->input->post('firstname')." ".$this->input->post('lastname'),
				'gender' => $this->input->post('gender'),
				'curr_city' => $this->input->post('curr_city'),
				'birthdate' => $this->input->post('birthdate'),
				'about_me' => $this->input->post('about_me'),
				'email' => $this->input->post('email'),
				'phone_no' => $this->input->post('phone_no'),
				'web_page' => $this->input->post('web_page'),
				'planguages' => $this->input->post('planguages'),
				'expert' => $this->input->post('expert'),
				'interests' => $this->input->post('interests'),
				'work_for' => $this->input->post('work_for'),
				'future_plans' => $this->input->post('future_plans')
			);
			
			$this->db->where('user_id', $user_id);
			$this->db->update('profile', $data);
		}
		
		public function set_profilepic($user_id,$image_name,$thumb_name)
		{
			$data = array(
			
				'profile_pic' => $image_name,
				'thumb_profile_pic' => $thumb_name
			);
			
			$this->db->where('user_id', $user_id);
			$this->db->update('profile', $data);
		}
		
		// for comp'09 awards
		public function set_voted($user_id){
			$this->db->query("update profile set voted = 1 where user_id =".$user_id);
		}
		
		public function is_voted($user_id){
			$this->db->select("voted");
			$query = $this->db->get_where("profile", array("user_id" => $user_id));
			return $query->row_array();
		}
		
	}
	
?>