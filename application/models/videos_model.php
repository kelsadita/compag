<?php

	/**
	 * 
	 */
	class Videos_model extends CI_Model {
		
		public function __construct() {
			$this->load->database();
		}
		
		public function get_videos_info($user_id = 0)
		{
			if($user_id != 0)
			{
				$this->db->order_by("video_add_datetime", "desc");
				$query = $this->db->get_where('videos', array('user_id' => $user_id));
			}
			else
			{
				$this->db->order_by("video_add_datetime", "desc");
				$query = $this->db->get('videos');
			}
			return $query->result_array();
		}
		
		public function get_video($video_id)
		{
			$query = $this->db->get_where('videos', array('video_id' => $video_id));
			return $query->row_array();
		}
		
		public function set_video($user_id, $video_title, $embed_url)
		{
			$now = date('Y-m-d H:i:s');
			$data = array(
			
				'user_id' => $user_id,
				'title' => $video_title,
				'url' => $embed_url,
				'video_add_datetime' => $now
			);
			
			$this->db->insert('videos', $data);
		}
		
		public function delete_video($video_id)
		{
			$this->db->where('video_id', $video_id);
			$this->db->delete('videos');
			
			$this->db->where('title_id', $video_id);
			$this->db->delete('recent_updates');
		}
	
	}
	
?>