<?php
	
	/**
	 * 
	 */
	class Sem_model extends CI_Model {
		
		public function __construct() {
			$this->load->database();
		}
		
		public function get_sem($user_id)
		{
			$this->db->select('sem');
			$query = $this->db->get_where('profile', array('user_id' => $user_id));
			return $query->row()->sem;
		}
		
		public function update_sem($user_id)
		{
			$this->db->where('user_id', $user_id);
			$data = array(
				'sem' => $this->input->post('sem')
			);
			
			$this->db->update('profile', $data);
		}
		
		public function get_notice($sem_id)
		{
			$query = $this->db->query("select * from notice where sem_id = $sem_id order by n_add_datetime desc");
			return $query->result_array();
		}
		
		public function get_subjects($sem_id)
		{
			$query = $this->db->get_where('subject', array('sem_id' => $sem_id));
			return $query->result_array();
		}
		
		public function get_timetable($sem_id)
		{
			$query = $this->db->query("select * from timetable$sem_id");
			return $query->result_array();
		}
		
		public function update_tt($sem_id)
		{
			if($this->input->post('lecture') != "Lecture(Room no.)")
			{
				$lecture = $this->input->post('lecture');
				$time = $this->input->post('time');
				$day = $this->input->post('day');
		
				$this->db->where('Time', $time);
				
				$data = array(
					$day => $lecture
				);
				
				$this->db->update("timetable$sem_id", $data);
				
				return 1;
			}
			else 
			{
				return 0;
			}		
		}
		
		public function set_notice($sem_id)
		{
			$now = date("Y-m-d H:i:s");
			$data = array(
				'sem_id' => $sem_id,
				'notice_title' => $this->input->post('notice_title'),
				'notice_des' => $this->input->post('notice'),
				'n_add_datetime' => $now
			);
			
			$this->db->insert('notice', $data);
		}
		
		public function get_test_cat($sem_id)
		{
			$query = $this->db->query("select distinct test_cat from exam_schd where sem_id = $sem_id");
			return $query->result_array();
		}
		
		public function get_test_data($sem_id, $category)
		{
			$query = $this->db->query("select * from exam_schd where test_cat = '$category' and sem_id=$sem_id order by tdate");
			return $query->result_array();
		}
		
		public function add_exam($sem_id)
		{
			$data = array(
				'sem_id' => $sem_id,
				'subject' => $this->input->post('subject'),
				'test_cat' => $this->input->post('test_cat'),
				'tdate' => $this->input->post('tdate'),
				'syllabus' => $this->input->post('syllabus'),
			);
			
			$this->db->insert('exam_schd', $data);
		}
		
		public function delete_test($test_id)
		{
			$this->db->where('test_id', $test_id);
			$this->db->delete('exam_schd');
		}
		
		public function delete_notice($nid)
		{
			$this->db->where('nid', $nid);
			$this->db->delete('notice');
		}
		
		public function get_sub_name($sub_id)
		{
			$query = $this->db->get_where('subject', array('sub_id' => $sub_id));
			return $query->row()->sub_name;
			
		}
		
		public function get_subjectdata($sub_id)
		{
			$query = $this->db->get_where('subject', array('sub_id'=> $sub_id));
			return $query->row_array();
		}
		
		public function get_experiments($sub_id)
		{
			$query = $this->db->get_where('experiments', array('sub_id' => $sub_id));
			return $query->result_array();
		}
		
		public function get_assignments($sub_id)
		{
			$query = $this->db->get_where('assignments', array('sub_id'=> $sub_id));
			return $query->result_array();
		}

		public function get_booksdata($sub_id)
		{
			$query = $this->db->get_where('ref_book', array('sub_id' => $sub_id));
			return $query->result_array();
		}
		
		public function add_assignment($sub_id, $assgn_title, $assgn_des, $submission_date, $assgn_add_date)
		{
			$data = array(
				'sub_id' => $sub_id,
				'assgn_title' => $assgn_title,
				'assgn_des' => $assgn_des,
				'submission_date' => $submission_date,
				'assgn_add_date' => $assgn_add_date
			);
			
			$this->db->insert('assignments', $data);
		}
		
		public function add_experiment($sub_id, $exp_title, $exp_des)
		{
			$data = array(
				'sub_id' => $sub_id,
				'exp_title' => $exp_title,
				'exp_des' => $exp_des
			);
			
			$this->db->insert('experiments', $data);
		}
		
		public function updt_description($sub_id, $description)
		{
			$this->db->where('sub_id', $sub_id);
			$data = array(
				'sub_id' => $sub_id,
				'description' => $description
			);
			$this->db->update('subject', $data);
		}
		
		public function add_refbook($sub_id)
		{
			$data = array(
				'book_name' => $this->input->post('book_name'),
				'sub_id' => $sub_id,
				'author' => $this->input->post('author'),
				'publication' => $this->input->post('publication'),
				'url' => $this->input->post('url')
			);
			
			$this->db->insert('ref_book', $data);
		}
		
		public function delete_experiment($exp_id)
		{
			$this->db->where('exp_id', $exp_id);
			$this->db->delete('experiments');
		}
		
		public function delete_assignment($assgn_id)
		{
			$this->db->where('assgn_id', $assgn_id);
			$this->db->delete('assignments');
		}
		
		public function delete_refbook($book_id)
		{
			$this->db->where('book_id', $book_id);
			$this->db->delete('ref_book');
		}
		
		public function get_files($categry)
		{
			$categry = str_replace("-", " ", $categry);
			$this->db->order_by("added_on", "desc");
			$query = $this->db->get_where('fshare', array('category' => $categry));
			return $query->result_array();
		}
		
		public function get_file_info($fid)
		{
			$query = $this->db->get_where('fshare', array('fid' => $fid));
			return $query->row_array();
		}
		
		public function set_file($user_id, $sem_id)
		{
			$now = date('Y-m-d H:i:s');
			$data = array(
				'sem_id' 	=> $sem_id,
				'user_id' 	=> $user_id,
				'title' 	=> $this->input->post('title'),
				'category' 	=> $this->input->post('category'),
				'description' 	=> $this->input->post('description'),
				'filelink' 	=> $this->input->post('link'),
				'added_on' 	=> $now
			);
			
			$this->db->insert('fshare', $data);
		}
	}
	
?>