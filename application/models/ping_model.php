<?php

	/**
	 * 
	 */
	class Ping_model extends CI_Model {
		
		function __construct() {
			$this->load->database();
		}
		
		public function get_pings($user_id)
		{
			$query = $this->db->query("select distinct sent_from from message where sent_to = $user_id order by sent_date_time desc");
			return $query->result_array();
		}
		
		public function get_from_pings($from_id, $user_id)
		{
			$query = $this->db->query("select * from message where sent_from = $from_id and sent_to = $user_id order by sent_date_time desc limit 1");
			return $query->row_array();
		}
		
		public function get_msg_count($from_id, $user_id)
		{
			$query = $this->db->query("select * from message where sent_from = $from_id and sent_to = $user_id and notice = 0");
			$count = $query->num_rows();
			return $count;
		}
		
		public function update_notice($from_id, $user_id)
		{
			$this->db->where('sent_from', $from_id);
			$this->db->where('sent_to', $user_id);
			$data = array('notice' => 1);
			$this->db->update('message', $data);
		}
		
		public function get_from_user_pings($from_id, $user_id)
		{
			$query = $this->db->query("SELECT * FROM message where (sent_to = $user_id and sent_from = $from_id) or (sent_to = $from_id and sent_from = $user_id) order by sent_date_time");
			return $query->result_array();
		}
		
		public function send_msg()
		{
			$to = $this->input->post('to');
			$message = $this->input->post('message');
			
			$query = $this->db->get_where('profile', array('username' => $to));
			
			if($query->num_rows() == 1)
			{
				foreach ($query->result() as $row) {
					$to_id = $row->user_id;
					$from_id = $this->session->userdata('user_id');
				}
				
				$now = date("Y-m-d H:i:S");
				$data = array(
					'sent_to' => $to_id,
					'sent_from' => $from_id,
					'msg' => $message,
					'sent_date_time' => $now,
					'notice' => 0
				);
				
				$this->db->insert('message', $data);
				
				$sent = 1;
				return $sent;
			}
	
			else {
				$sent = 0;
				return $sent;
			}
		}
	}
	
?>