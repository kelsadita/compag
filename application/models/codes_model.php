<?php

	/**
	 * 
	 */
	class Codes_model extends CI_Model {
		
		public function __construct() {
			$this->load->database();
		}
		
		public function get_codes_categories($user_id = 0)
		{
			if($user_id != 0)
			{
				$query = $this->db->query("select distinct code_category from codes where user_id = ".$user_id);
			}
			else 
			{
				$query = $this->db->query("select distinct code_category from codes");	
			}
			
			return $query->result_array();
		}
		
		public function get_code_list($category)
		{
			$query = $this->db->get_where('codes', array('code_category' => $category));
			return $query->result_array();
		}
		
		public function get_code($code_id)
		{
			$query = $this->db->get_where('codes', array('code_id' => $code_id));
			$row = $query->row_array();
			
			$user_query = $this->db->query("select username from profile where user_id = ".$row['user_id']);
			$user_row = $user_query->row_array();
			
			$row['username'] = $user_row['username'];
			
			return $row;
		}
		
		public function set_code($user_id)
		{
			$now = date('Y-m-d H:i:s');
			$data = array(
				
				'user_id' => $user_id,
				'code_category' => $this->input->post('code_category'),
				'code_title' => $this->input->post('code_title'),
				'code' => $this->input->post('code'),
				'code_add_datetime' => $now
			);
			
			$this->db->insert('codes', $data);
		}
		
		public function delete_code($user_id, $code_id)
		{
			$this->db->where('code_id', $code_id);
			$this->db->where('user_id', $user_id);
			$this->db->delete('codes');
			$this->db->where('title_id', $code_id);
			$this->db->where('user_id', $user_id);
			$this->db->delete('recent_updates');
		}
	}
	
?>