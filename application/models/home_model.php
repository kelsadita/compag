<?php

	/**
	 * 
	 */
	class Home_model extends CI_Model {
		
		function __construct() {
			$this->load->database();
		}
		
		public function get_feeds($limit, $offset)
		{	
			$query = $this->db->select('*')
					->from('recent_updates')
					->order_by('post_add_datetime', 'desc')
					->limit($limit, $offset);
					
			return $query->get()->result_array();
		}
		
		public function countrows()
		{
			$query = $this->db->get('recent_updates');
			return $query->num_rows();
		}
	}
	
?>