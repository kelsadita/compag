<?php

	/**
	 * 
	 */
	class Awards_model extends CI_Model {
		
		function __construct() {
			$this->load->database();
		}
		
		public function get_awards_names(){
			$query = $this->db->get('awards');
			return $query->result_array();
		}
		
		public function get_users_info(){
			$this->db->order_by("fname", "asc");
			$query = $this->db->get('award_user');
			return $query->result_array();
		}
		
		public function get_jodis_info(){
			$query = $this->db->get('award_jodi');
			return $query->result_array();
		}
		
		public function get_dostanas_info(){
			$query = $this->db->get('award_dostana');
			return $query->result_array();
		}
		
		public function from_comp($user_id){
			$this->db->select("comp");
			$query = $this->db->get_where("profile", array("user_id" => $user_id));
			return $query->row_array();
			
		}
		
		public function add_votes(){
			$user_id = $this->input->post('1');
			$this->db->query('update award_user set aw1 = aw1 + 1 where user_id = '.$user_id);
			
			$jid = $this->input->post('2');
			$this->db->query('update award_jodi set count = count + 1 where jid = '.$jid);
			
			$user_id = $this->input->post('3');
			$this->db->query('update award_user set aw3 = aw3 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('4');
			$this->db->query('update award_user set aw4 = aw4 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('5');
			$this->db->query('update award_user set aw5 = aw5 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('6');
			$this->db->query('update award_user set aw6 = aw6 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('7');
			$this->db->query('update award_user set aw7 = aw7 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('8');
			$this->db->query('update award_user set aw8 = aw8 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('9');
			$this->db->query('update award_user set aw9 = aw9 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('10');
			$this->db->query('update award_user set aw10 = aw10 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('11');
			$this->db->query('update award_user set aw11 = aw11 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('12');
			$this->db->query('update award_user set aw12 = aw12 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('13');
			$this->db->query('update award_user set aw13 = aw13 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('14');
			$this->db->query('update award_user set aw14 = aw14 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('15');
			$this->db->query('update award_user set aw15 = aw15 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('16');
			$this->db->query('update award_user set aw16 = aw16 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('17');
			$this->db->query('update award_user set aw17 = aw17 + 1 where user_id = '.$user_id);
			
			$did = $this->input->post('18');
			$this->db->query('update award_dostana set count = count + 1 where did = '.$did);
			
			$user_id = $this->input->post('19');
			$this->db->query('update award_user set aw19 = aw19 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('20');
			$this->db->query('update award_user set aw20 = aw20 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('21');
			$this->db->query('update award_user set aw21 = aw21 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('22');
			$this->db->query('update award_user set aw22 = aw22 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('23');
			$this->db->query('update award_user set aw23 = aw23 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('24');
			$this->db->query('update award_user set aw24 = aw24 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('25');
			$this->db->query('update award_user set aw25 = aw25 + 1 where user_id = '.$user_id);
			
			$user_id = $this->input->post('26');
			$this->db->query('update award_user set aw26 = aw26 + 1 where user_id = '.$user_id);
		}
	
	}
	
?>