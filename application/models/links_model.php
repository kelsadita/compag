<?php
	
	/**
	 * 
	 */
	class Links_model extends CI_Model {
		
		public function __construct() {
			$this->load->database();
		}
		
		public function get_link_categories($user_id = 0)
		{
			if($user_id != 0)
			{
				$query = $this->db->query("select distinct category from link where user_id = ".$user_id);
			}
			else 
			{
				$query = $this->db->query("select distinct category from link");	
			}
			
			return $query->result_array();
		}
		
		public function get_link($link_id)
		{
			$query = $this->db->get_where('link', array('link_id' => $link_id));
			return $query->row_array();
		}
		
		public function get_cat_content($category)
		{
			$query = $this->db->get_where('link', array('category' => $category));
			return $query->result_array();
		}
		
		public function set_link($user_id)
		{
			$now = date('Y-m-d H:i:s');
			$data = array(
				
				'user_id' => $user_id,
				'title' => $this->input->post('title'),
				'category' => $this->input->post('category'),
				'url' => $this->input->post('url'),
				'description' =>$this->input->post('description'),
				'add_date_time' => $now
				
			);
			
			$this->db->insert('link', $data);
		}
		
		public function delete_link($user_id, $link_id)
		{
			$this->db->where('link_id', $link_id);
			$this->db->where('user_id', $user_id);
			$this->db->delete('link');
			$this->db->where('title_id', $link_id);
			$this->db->where('user_id', $user_id);
			$this->db->delete('recent_updates');
		}
	}
	
	
?>