<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//home
$route['home/index'] = 'home/index/$1';

//profile 
$route['profile/view'] = 'profile/view/$1';
$route['profile/edit'] = 'profile/edit/$1';
$route['profile/update'] = 'profile/update/$1';
$route['profile/members'] = 'profile/members';
$route['profile/albums'] = 'profile/albums/$1';
$route['profile/edit_album'] = 'profile/edit_album/$1';
$route['profile/codes'] = 'profile/codes/$1';
$route['profile/delete_code'] = 'profile/delete_code/$1';
$route['profile/discussions'] = 'profile/discussions/$1';
$route['profile/links'] = 'profile/links/$1';
$route['profile/delete_link'] = 'profile/delete_link/$1';

//links
$route['links'] = 'links/index';
$route['links/category'] = 'links/category/$1';

$route['default_controller'] = 'login';
$route['login/reset'] = 'login/reset';
$route['login/updt_password'] = 'login/updt_password/$1';
$route['login/updt_password'] = 'login/change_password/$1';
$route['login/remote_login'] = 'login/remote_login/$1';
$route['login/index'] = 'login/index/$1';

//codes
$route['codes'] = 'codes/index';
$route['codes/add_code'] = 'codes/add_code';
$route['codes/category'] = 'codes/category/$1';
$route['codes/view'] = 'codes/view/$1';

//photos
$route['photos'] = 'photos/index';
$route['photos/view'] = 'photos/view/$1';
$route['photos/upload_photo'] = 'photos/upload_photo/$1';
$route['photos/edit'] = 'photos/edit/$1/$2';
$route['photos/edit_album'] = 'photos/edit_album/$1/$2';
$route['photos/delete_photo'] = 'photos/delete_photo/$1/$2';

//videos
$route['videos'] = 'videos/index';
$route['videos/add'] = 'videos/add';

//discussions
$route['discuss/index'] = 'discuss/index/$1/$2';
$route['discuss/add'] = 'discuss/add';
$route['discuss/view'] = 'discuss/view/$1';
$route['discuss/addans'] = 'discuss/addans/$1';
$route['discuss/ans_response'] = 'discuss/ans_response/$1/$2/$3';

//pings
$route['pings'] = 'pings/index';
$route['pings/view'] = 'pings/view/$1';
$route['pings/send_msg'] = 'pings/send_msg';

//semester
$route['semester/update'] = 'semester/update/$1';
$route['semester'] = 'semester/index';
$route['semester/update_sem'] = 'semester/update_sem';
$route['semester/timetable'] = 'semester/timetable';
$route['semester/update_tt'] = 'semester/update_tt/$1';
$route['semester/tests'] = 'semester/tests';
$route['semester/add_test'] = 'semester/add_test/$1';
$route['semester/delete_test'] = 'semester/delete_post/$1';
$route['semester/subject'] = 'semester/subject/$1';
$route['semester/delete_enteries'] = 'semester/delete_enteries/$1';
$route['semester/updt_subject'] = 'semester/updt_subject/$1';
$route['semester/files'] = 'semester/files/$1';
$route['semester/view_files'] = 'semester/view_files/$1/$2';
$route['semester/download'] = 'semester/download/$1';

//other
$route['semester/error'] = 'semester/error';
/* End of file routes.php */
/* Location: ./application/config/routes.php */