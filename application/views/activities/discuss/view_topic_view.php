<script type="text/javascript" src="<?php echo base_url();?>scripts/nicEdit.js"></script>
<script type="text/javascript">
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".like").click(function(){
			var ans_id = $(this).attr("id");
			$("."+ans_id).load('<?php echo base_url();?>discuss/ans_response/<?php echo $this->session->userdata('user_id');?>/'+ans_id+'/<?php echo $topic_info['did'];?>');
		});
	});
</script>
<div class = "linkcontent" style = "width:800px">
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
<?php 
		
		$topic_user_id = $topic_info['user_id'];
		$topic = $topic_info['topic'];
		$description = $topic_info['description'];
		$num_ans = $topic_info['num_ans'];
		$did = $topic_info['did'];
		
		$userinfo = $this->login_model->get_user_info($topic_user_id);
		$username = $userinfo['username'];
		$path = PP_UPLOADPATH.$userinfo['thumb_profile_pic'];
		
		echo '<h2>'.$topic.'</h2><hr noshade style="border: 2px solid #CCCCCC;">';
		echo '<table><tr>';
		echo '<td style = "vertical-align:top;"><image src = '.$path.' width = "55" height = "55"></td>';
		echo '<td><b class = "fade">added by '.$username.'</b><br /><br />'.$description.'</td>';
		echo '</tr></table><hr noshade style="border: 1px solid #CCCCCC;">';
		
		if($num_ans != 0)
        	{
        		echo '<p class="heading">Answers</p>';
        	}
		
		foreach ($answers_data as $answer_data) {
			
			$ans_id = $answer_data['ans_id'];
			$ans_user_id = $answer_data['user_id'];
			$userdata = $this->login_model->get_user_info($ans_user_id);
			$username = $userdata['username'];
			$profile_pic = $userdata['thumb_profile_pic'];
			$path = PP_UPLOADPATH.$profile_pic;
				
				
			$ans = $answer_data['ans'];
			$ans_id = $answer_data['ans_id'];
			
			$num_responses = $this->discuss_model->get_num_likes($ans_id);
				
   			echo '<table style = " border-bottom: 1px solid #CCCCCC;"><tr>';
			echo '<td style = "vertical-align:top; width:60px;"><image src = '.$path.' width = "55" height = "55"></td>';
			echo '<td><b><a class = "a" >'.$username.'</a></b><b class = "fade" style = "float:right;">'.date('F j,Y h:i A', strtotime($answer_data['ans_add_datetime'])).'
			</b><br /><p style="width:550px; word-wrap: break-word;">'.$ans.'</p></td></tr><tr><td></td><td><p class="'.$ans_id.'" style="float: left;"><b class="fade">'.$num_responses.' liked it</b></p>
			<p class="like"  id="'.$ans_id.'"><img src="'.IMAGES_BASE.'rock.png">like</p></td></p>';
			echo '</tr></table>';
		}
		
		echo '<p class="heading">Add Answer</p>';
?>
<form method = "post" action = "<?php echo base_url();?>discuss/addans/<?php echo $did;?>" style="margin-left: 50px;">
<table>
    	<tr><td><textarea name ="ans" style ="width: 550px; height: 200px;" ></textarea></td></tr>
       	<tr><td align="right"><input type="submit" class = "button" value="Add Answer" name = "submit" style = "margin-top: 15px; width: 100px;"/></td></tr>
</table>
</form>
</td>
</tr>
</table>
</div>
