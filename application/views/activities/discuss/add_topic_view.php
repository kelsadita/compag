<script type="text/javascript" src="<?php echo base_url();?>scripts/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>

<div class = "linkcontent">
<table width = "100%" style = "margin-top : 30px;border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Add Topic</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
		<?php 
			
			if ($this->session->flashdata('notice')) {
				echo "<p class=notify>".$this->session->flashdata('notice')."</p>";
			}
		?>
		<form action="<?php echo base_url()?>discuss/add" method="post" id="adddata">
	
		<table cellpadding = "8" style = "margin:0 auto;">
		<tr>
			<td class = "fade">Topic</td>
			<td><input type = "text" class="required" id = "topic" name = "topic"/></td>
		</tr>
		<tr>
			<td class = "fade">Description</td>
        	<td><textarea name ="description" style ="width: 500px; height: 350px;"></textarea></td>
		</tr>
		<tr>
        	<td></td>
			<td><input type="submit" class = "button" style = "width: 70px;" value="submit" name = "submit"/></td>
		</tr>
		</table>
		</form>
	</td>
	</tr>
</table>
</div>
