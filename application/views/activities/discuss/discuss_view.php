<div class = "linkcontent">
<table width = "100%" style = "margin-top : 30px;border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Discussions</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
<?php 
	
	echo '<a href="'.base_url().'discuss/most_recent" style="margin-right: 10px;"';
	if ($ref == 'most-recent') 
	{
		echo 'class="hbutton"';	
	}
	echo '>most recent</a>';
	echo '<a href="'.base_url().'discuss/most_discussed"';
	if ($ref == 'most-discussed') 
	{
		echo 'class="hbutton"';
	}
	echo '>most discussed</a>	
		  <a href = "'.base_url().'discuss/add" class = "hbutton" style = "margin-left: 300px;">Add Topic</a>
	      <br /><br /><hr noshade style = "2px solid #CCCCCC;" />';
	if($this->session->flashdata('notice'))
	{
		echo "<p class=notify>".$this->session->flashdata('notice')."</p>";
	}
	
    foreach ($discussions_data as $discussion_data) {
	
		$user_id = $discussion_data['user_id'];
		$topic = $discussion_data['topic'];
		$did = $discussion_data['did'];
		$num_ans = $discussion_data['num_ans'];
        $description = strip_tags($discussion_data['description']);
                        
        if(strlen($description)<400)
       	{
			$descriptstr = $description;
		}
		else
		{
			$descriptstr = substr($description, 0, 400)."....<a href =".base_url()."discuss/view/$did>(more)</a>";
		}
                      
		$userdata = $this->login_model->get_user_info($user_id);
		$profile_pic_name = $userdata['profile_pic'];
		$path = PP_UPLOADPATH.$profile_pic_name;
		
 		echo '<table style = "border-bottom:1px solid #CCCCCC; width:600px; margin:0 auto;"><tr>';
		echo "<td width = '60'><image src = $path width = 55 height = 55></td>";
		echo '<td><b class = "fade" style = "float:right;">'.date('F j,Y h:i A', strtotime($discussion_data['add_date_time'])).'</b>
                <b style="font-size:15px;"><a class = a href = "'.base_url().'discuss/view/'.$did.'">'.$topic.'</a></b><br/><b class = "fade">added by&nbsp;<a class = "fade" href = '.base_url().'profile/view/'.$user_id.'>'.$userdata['username'].'</a></b>
                <br /><p>'.$descriptstr.'</p><b class = "fade">Comments</b>:'.$num_ans.'</td>';
		echo '</tr></table>';
	}

	if(strlen($pagination))
	{
		echo '<div class = "pagination"><center>'.$pagination.'</center></div>';	
		
	}
		
	
        
     
?>
	</td>
	</tr>
</table>
</div>