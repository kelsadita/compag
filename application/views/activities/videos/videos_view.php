<div class = "linkcontent" >
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar'); ?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Videos</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />


<button style="float:right;" ><img src="<?php echo IMAGES_BASE;?>video.png">&nbsp;Add Video</button>

<div id ="add" style ="display:none;">

<form action="<?php echo base_url();?>videos/add" method="post" id ="adddata">
<table cellpadding="7" cellspacing="7">
    <tr>
        <td>
            Video Title:    
        </td>
        <td>
            <input name="video_title" id="video_title" type="text" maxlength="50" class ="required"/>        
        </td>
    </tr>
    <tr>
        <td>
            URL:      
        </td>
        <td>
            <input type="text" id="url" name="url" maxlength="100" class ="required"/>    
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
              <h4 style="margin-left: 15px;">eg. http://www.youtube.com/watch?v=8UlH0U-wdGE<br /><br /></h4>
              <input name="share" type="submit" value="Share" class ="button" style="margin-left: 12px;" />
        </td>
    </tr>
</table>

</form>
<hr noshade style ="border: 2px solid #CCCCCC "/>
</div>
<br />
<br />
<br />

<?php
if($this->session->flashdata('notice'))
{
	echo "<p class=notify>".$this->session->flashdata('notice')."</p>";
}

echo '<table width="100%" style = "margin-top:70px;" class = "table">';
foreach ($videos_info as $video_info) {

	$user_id = $video_info['user_id'];
	$username = $this->login_model->get_user_info($user_id);
    echo "
    <tr>
        <td width='300' height='200' style='border-bottom:1px solid #DBDBDB;'>
        <iframe width='300' height='200' src='http://www.youtube.com/embed/".$video_info['url']."' frameborder='0' allowfullscreen></iframe>
        <br />
        <br />
        </td>
        <td style='border-bottom:1px solid #DBDBDB;'>
        <b class = fade>&nbsp;&nbsp;added by</b><a href = profile/view/$user_id>&nbsp;&nbsp;".$username['username']."</a>";
    echo "
        </td>
            
    </tr>
    
    ";
    
}
echo "</table>";
?>
        </td>
        </tr>
</table>
</div>