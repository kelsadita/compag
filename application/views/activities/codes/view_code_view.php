<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/prettify.css">
<script type="text/javascript" src="<?php echo base_url(); ?>scripts/prettify.js"></script>
</head>
<body onload="prettyPrint()">

<div class = "wrapper" style = "min-height:700px; margin-bottom:30px;">
<?php $code_add_datetime = date('F j,Y h:i A', strtotime($code_info['code_add_datetime'])); ?>
<table cellpadding = "5">
	<tr><td class = "fade">Program</td><td><?php echo $code_info['code_title']; ?></td></tr><br />
	<tr><td class = "fade">Author</td><td><?php echo $code_info['username']; ?></td></tr><br />
	<tr><td class = "fade">Dated</td><td><?php echo $code_add_datetime; ?></td></tr>
</table>		
		
<pre class="prettyprint" style = "padding-left: 25px;">
	<?php echo "\n".$code_info['code'];?>
</pre>

</div>