<script type="text/javascript" src="<?php echo base_url();?>scripts/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>

<div class = "linkcontent" style = "width: 800px;">
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Code Categories</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
		
		<p style = "float:right;" class="hbutton"><a href="<?php echo base_url();?>codes" style="color: white;">back</a></p><br /><br /><br />
		<form action="<?php echo base_url();?>codes/add_code" method="post">
			<table cellpadding = "8">
				<tr>	
					<td class = "fade">Category</td>
					<td>
						<select style = "height: 25px;" id ="code_category" name ="code_category">
	    					<option>C++</option>
	    					<option>Java</option>
	    					<option>PHP</option>
	    					<option>Data Structure and Files</option>
	   		 			<option>Computer Graphics</option>
	    					<option>Operating System</option>
	    					<option>Analysis Of Algorithm And Design</option>
	    					<option>Microprocessor</option>
	    					<option>Computer Networks</option>
	    					<option>Web Engineering</option>
	    					<option>Advance Microprocessor</option>
	    					<option>System Programming and Compiler Construction</option>
	    					
						</select>
					</td>	
				</tr>	
				<tr>
					<td class = "fade">Title</td>
					<td><input type = "text" id = "code_title" name = "code_title" style = "width:490px;"/></td>
				</tr>
				<tr>	
					<td class = "fade">Program</td>
					<td>
						<textarea name="code" rows="20" cols="60"></textarea>								
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" class = "button" style = "width: 70px;" value="submit" name = "submit"/></td>
				</tr>
			</table>
		</form>
		
		</td>
	</tr>
</table>

</div>