<div class = "linkcontent" >
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar') ?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Link Categories</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />

<button style="float:right;" ><img src="<?php echo IMAGES_BASE;?>world_link.png">&nbsp;Add Link</button>
<br /><br /><br />
	<?php
			if($this->session->flashdata('success'))
			{
				echo "<p class = notify>".$this->session->flashdata('success')."</p>";
			}
	?>
	<div id = "add" style = "display:none">
	<?php 
		$attributes = array('id' => 'adddata');
		echo form_open('links/add_link', $attributes);	
	?>
	
		<label for ="category">Category: </label>
		<select id ="category" name ="category">
		    <option>Programming</option>
		    <option>References</option>
		    <option>Downloads</option>
		    <option>Q and A sites</option>
		    <option>Online Tutorials</option>
		    <option>Books</option>
			<option>Mumbai University</option>
			<option>Other</option>
		</select><br /><br />
		<label for ="title">Title:</label>
		<input class = "required" id ="title" name ="title" type ="text" /><br />
		<label for ="url">Url:</label>
		<input class = "required url" id ="url" name ="url" type ="text" /><br />
		<label for ="description">Description:</label>
		<textarea class = "required" id="description" name="description" rows = "5" cols = "24"></textarea><br /><br />
		<label>&nbsp;</label>
		<input type ="submit" value ="Add" name ="submit" class = "button" /><br />
	</form>
	<br />
	<hr />
	</div>
	<?php 
		
		$path = IMAGES_BASE."link_cat.png";
		foreach ($links_categories as $link_category)
		{
			$category = $link_category['category'];
			echo '<a href = links/category/'.str_replace(" ", "-", $link_category['category']).'>';
			echo '<table class = "updt_table" ><tr>';
			echo '<td style = "width:60px;"><image alt = '.$path.' src = '.$path.' width = 55 height = 55></td>';
			echo '<td style = "font-size: 13px;">'.$category;
			echo '</tr></table></a>';
		}	
	?>
	</td>
	</tr>
</table>
</div>