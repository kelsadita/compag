<div class = "linkcontent">
<table width = "100%" style = "margin-top : 30px;border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Recent Albums</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />

<button style="float:right;"><img src="<?php echo IMAGES_BASE;?>photo_album.png"/>&nbsp;Create Album</button>

<br /><br /><br />
<div id = "add" style="display: none;">
<br /><br />
<form method ="post" id = "adddata" action ="photos/add_album">
	<label for ="album_name">Album Name* : </label>
	<input class = "required" name="album_name" id="album_name" type="text"/><br />
	<label for ="description">Description : </label>
	<textarea id="description" name="description" rows="5" cols="23"></textarea><br /><br />
	<label>&nbsp;</label>
	<input type ="submit" class = "button" value ="Add" name ="create" /><br /><br />
</form>
</div>
<?php
	
	
	foreach ($albums_info as $album_info) {
		
		$album_id = $album_info['album_id'];
		$album_add_datetime = date('F j,Y', strtotime($album_info['album_add_datetime']));
		$album_user_id = $album_info['user_id'];
		$album_description = $album_info['description'];
		$name = $album_info['name'];

		$username = $this->login_model->get_user_info($album_user_id);
		$cover_pic_name = $this->photos_model->get_cover($album_id);
		
		$path = ALBUM_PHOTOS_PATH.$cover_pic_name;
		echo '<table class = "updt_table"><tr>';
		echo '<td style = "width:60px;"><image alt = '.$path.' src = '.$path.' width = 55 height = 55></td>';
		echo '<td style = "font-size: 13px;"><a href = photos/view/'.$album_id.'>'.$name.'</a>';
		echo '<b class  = "fade"> by </b><a class = "a" href = "profile/view/'.$album_user_id.'">'.$username['username'].'</a><b class = "fade" style = "float:right;">'.$album_add_datetime.'</b>';
		echo '<br/></br><b class = "fade">'.$album_description;
		echo '</tr></table>';
	}

?>


	</td>
	</tr>
</table>
</div>