<div class = "linkcontent">
<h2>Edit Photo</h2>
<a href="<?php echo base_url(); ?>/photos/view/<?php echo $photo_data['album_id']; ?>" style="float:right;border-radius:15px;-moz-border-radius:15px;background-color: #99CCCC;color: white;text-decoration: none; font-weight: bold;padding:5px;margin-top:7px;" >Back</a>
<br />
<hr />
<br />

<form id = "adddata" action="<?php echo base_url();?>/photos/edit/<?php echo $photo_data['album_id']."/".$photo_data['photo_id']?>" method="post">
<table cellpadding="7" cellspacing="7">
   	<tr>
        <td>
            Photo Name:    
        </td>
        <td>
            <input class = "required" name="photo_name" id="photo_name" type="text" value="<?php echo $photo_data['pname']; ?>" maxlength="100"/>        
        </td>
    </tr>
    <tr>
        <td>
            Description:       
        </td>
        <td>
            <textarea id="description" name="description" maxlength="100" cols="25" rows="4"><?php echo $photo_data['pdescription']; ?></textarea>    
        </td>
    </tr>
</table>
<input name="save_changes" type="submit" value="Save Changes" style="margin-left: 12px;" />
</form>
</div>