<script>
	$(document).ready(function(){
	
		$("#birthdate").datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
    		changeYear: true,
    		yearRange: '1985:2000'
		});
			
	});
</script>
<style type="text/css">
form label{ width: 200px;}
label.error { width:200px; float: none; color: red; vertical-align:top;}
</style>
<div class = "linkcontent" >
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar') ?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Edit Profile</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />

	<?php
	
	//retrieving the firname and the lastname of the user.
	
	$username = explode(" ", $profile_data['username']);
	$firstname = $username[0];
	$lastname = $username[1];
	
	if ($this->session->flashdata('notice')) {
		echo '<p class="notify">'.$this->session->flashdata('notice').'</p>';
	}
	?>

 <form enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>profile/update/<?php echo $user_id; ?>" id = "adddata">
  
     <p class = "heading">Basic Information</p><br />
     <label for="firstname">Firstname:</label>
     <input class = "required" type="text" id="firstname" name="firstname" value="<?php if (!empty($firstname)) echo $firstname; ?>" /><br />
     <label for="lastname">Lastname:</label>
     <input type="text" id="lastname" name="lastname" value="<?php if (!empty($lastname)) echo $lastname; ?>" /><br />
     <label for="gender">Gender:</label>
     <select id="gender" name="gender">
        <option value="Male" <?php if (!empty($profile_data['gender']) && $profile_data['gender'] == 'Male') echo 'selected = "selected"'; ?>>Male</option>
        <option value="Female" <?php if (!empty($profile_data['gender']) && $profile_data['gender'] == 'Female') echo 'selected = "selected"'; ?>>Female</option>
     </select><br /><br />
     <label for = "curr_city">Current city : </label>
     <input type="text" id="curr_city" name="curr_city" value="<?php if (!empty($profile_data['curr_city'])) echo $profile_data['curr_city']; ?>" /><br />
     <label for="birthdate">Birthdate:</label>
     <input class = "required date" type="text" id="birthdate" name="birthdate" value="<?php if (!empty($profile_data['birthdate'])) echo $profile_data['birthdate'];?>" /><br />
     <label style = "vertical-align:top" for="about_me">Bio:</label>
     <textarea rows = "3" cols = "30" id="about_me" name="about_me" ><?php if (!empty($profile_data['about_me'])) echo $profile_data['about_me']; ?></textarea><br />
     <p class = "heading">Contact Information</p><br />
     <label for="email">Email:</label>
     <input class = "required email" type="text" id="email" name="email" value="<?php if (!empty($profile_data['email'])) echo $profile_data['email']; ?>" /><br />
     <label for="phone_no">Phone No.:</label>
     <input type="text" id="phone_no" name="phone_no" value="<?php if (!empty($profile_data['phone_no'])) echo $profile_data['phone_no']; ?>" /><br />
     <label for="web_page">Web Page:</label>
     <input type="text" id="web_page" name="web_page" value="<?php if (!empty($profile_data['web_page'])) echo $profile_data['web_page']; ?>" class="url"/><br />
     <p class = "heading">Other</p><br />
     <label for="planguages">Prog. languages known : </label>
     <input type="text" id="planguages" name="planguages" value="<?php if (!empty($profile_data['planguages'])) echo $profile_data['planguages']; ?>" /><br />
     <label for="expert">Expert In:</label>
     <input type="text" id="expert" name="expert" value="<?php if (!empty($profile_data['expert'])) echo $profile_data['expert']; ?>" /><br />
     <label for="interests">Interested In:</label>
     <input type="text" id="interests" name="interests" value="<?php if (!empty($profile_data['interests'])) echo $profile_data['interests']; ?>" /><em class = "fade">web designing etc.</em><br />
     <label for="future_plans">Will like to work for:</label>
     <input type="text" id="work_for" name="work_for" value="<?php if (!empty($profile_data['work_for'])) echo $profile_data['work_for']; ?>" /><em class = "fade">Google Microsoft etc.</em><br />
     <label for="future_plans">Future Plans:</label>
     <input type="text" id="future_plans" name="future_plans" value="<?php if (!empty($profile_data['future_plans'])) echo $profile_data['future_plans']; ?>" /><em class = "fade">M.tech MBA etc.</em><br />
     <hr />
   
     <input class = "button" type="submit" value="Save Changes" name="submit" style = "float : right; width: 110px; margin-bottom:40px;" />
  </form>
	</td>
	</tr>
</table>
</div>