<script type="text/javascript">
	$(document).load(function(){
	$(".edit").colorbox({inline: true});
	});
</script>
<div class = "linkcontent" style = "padding-top:20px;width:600px;">
<a href="<?php echo base_url();?>profile/view/<?php echo $album_user_id; ?>" class="hbutton" style = "float: right;">back</a>
<?php 

if($user_id != $album_user_id)
{
	$user_data = $this->login_model->get_user_info($album_user_id);
	$album_user_name = $user_data['username'];
	
	echo '<h2>'.$album_user_name.'\'s Albums</h2><br /><hr noshade style="border:2px solid #CCCCCC;"/>';
}
else 
{
	echo '<h2>My Albums</h2><br /><hr noshade style="border:2px solid #CCCCCC;"/>';
}

if ($this->session->flashdata('notice')) 
{
	echo '<p class = "notify">'.$this->session->flashdata('notice').'</p>';	
}


if(!empty($user_album_info))
{

echo "<table width='100%'>";
foreach ($user_album_info as $album_info) 
{
	$cover_pic_name = $this->photos_model->get_cover($album_info['album_id']);
    echo "
    <tr>
        <td width='110px' style='border-bottom:1px solid #DBDBDB;'>
        <a href='".base_url()."photos/view/".$album_info['album_id']."'>
        <img src='".ALBUM_PHOTOS_PATH.$cover_pic_name."' width='100' height='100' /></a>
        </td>
        <td style='border-bottom:1px solid #DBDBDB;'>
        <b style='color:#3b608a;'>".$album_info['name']."</b><br/><p class = fade>
        ".$album_info['description']."</p>
        </td>";
     
    if($album_user_id == $user_id)
    {
    	echo" 
       	<td style='border-bottom:1px solid #DBDBDB;'>
        <a class = 'edit' href='".base_url()."profile/edit_album/".$album_info['album_id']."'  style=float:right;'>Edit</a><br /><br />
        <a href='".base_url()."profile/delete_album/".$album_info['album_id']."/".$user_id."' style='float:right;'>Delete</a>
        </td>
            
    	</tr>";
    }
    
}
echo "</table>";
}
else
{
    echo "<br /><br /><h2><center>No albums found!</center></h2>";
}
?>
</div>