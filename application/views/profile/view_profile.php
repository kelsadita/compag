<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/tipsy.css" />

<SCRIPT LANGUAGE="JavaScript" SRC="<?php echo base_url();?>scripts/jquery.tipsy.js"></SCRIPT>

<script type="text/javascript">
	$(document).ready(function(){
	
	$("#edit").tipsy({gravity: 'sw'});
	$(".image1").colorbox({rel:'image1',width:"60%", height:"80%"});
	$(".inline").colorbox({inline:true, width:"30%"});
	});
</script>

<?php
	$main_pic = $profile_data['profile_pic'];
	$profile_pic = $profile_data['thumb_profile_pic'];
	$path = PP_UPLOADPATH.$profile_pic;
?>

<div class = "linkcontent" style = "padding-top:20px;">


	<b class="a" style="font-size: 18px;"><?php echo htmlspecialchars($profile_data['username']);?></b>&nbsp;
	<?php 
	if($user_id == $this->session->userdata('user_id'))
	{
	?>
	<a id ="edit" title = "edit profile" href="<?php echo base_url();?>profile/edit/<?php echo $user_id;?>"><img src="<?php echo IMAGES_BASE;?>editprofile.png"/></a>
	<?php
	}
	?>
	<br /><br />
	<table>
	<tr>
	<td style= "border-right:1px solid #DBDBDB; height:700px; width:170px; ">
		 <a class='image1' title='' href='<?php echo PP_UPLOADPATH.$main_pic;?>'> 
		<img style="border : 3px solid #E5E5E5;" alt="<?php echo $profile_pic;?>" src="<?php echo $path;?>" width = "170" height = "200"/><br />
		</a>
		<?php if($profile_data['user_id'] == $this->session->userdata('user_id'))
		{
		?> 
		<div style="background-color: #E5E5E5; height: 25px;">&nbsp;<a class='inline' href="#inline_content">Change Picture</a></div>
		<div style="display: none; width: 350px;">
		<div id="inline_content" style="width: 350px; margin: 0 auto;">
			<form enctype="multipart/form-data" action="<?php echo base_url()?>profile/update_pic/<?php echo $user_id;?>" method="post">
				<input type="file" name="profile_pic" />
				<input type="submit" value="change" />
			</form>
		</div>
		</div>
		<?php 
		}
		?>
		<br />
		<?php if($profile_data['about_me'] != " " && !empty($profile_data['about_me'])) echo '<p style = "border:1px solid #A9A9A9;padding : 3px;border-radius:5px;-moz-border-radius:5px;">'.$profile_data['about_me'].'</p>'; ?>	
		<br /><br />
		<img src = "<?php echo IMAGES_BASE; ?>photo_album.png">&nbsp;<a href = "<?php echo base_url();?>profile/albums/<?php echo $user_id ?>">Photos</a>
		<br /><br />
		<img src = "<?php echo IMAGES_BASE; ?>video.png">&nbsp;<a href = "<?php echo base_url();?>profile/videos/<?php echo $user_id ?>">Videos</a>
		<br /><br />
		<img src = "<?php echo IMAGES_BASE; ?>code.png" />&nbsp;<a href = "<?php echo base_url();?>profile/codes/<?php echo $user_id ?>">Codes</a>
		<br /><br/>
		<img src = "<?php echo IMAGES_BASE; ?>discuss.png" />&nbsp;<a href = "<?php echo base_url();?>profile/discussions/<?php echo $user_id ?>">Discussions</a>
		<br /><br/>
		<img src = "<?php echo IMAGES_BASE; ?>world_link.png" />&nbsp;<a href = "<?php echo base_url();?>profile/links/<?php echo $user_id ?>">Links</a>
	</td>
		
	<td style = "width:550px;">
		<p class = "heading" >Basic Information</p><br /><br/>
		<table cellpadding = "7" >
		<tr><td><b class= "vpelements" >Sex  </b></td><td><?php echo $profile_data['gender'];?></td></tr>
		<tr><td><b class= "vpelements" >Birthdate  </b></td><td><?php echo date('F j,Y', strtotime($profile_data['birthdate']));?></td></tr>
		<tr><td><b class= "vpelements" >Lives In  </b></td><td><?php echo $profile_data['curr_city'];?></td></tr>
		</table>
		<p class = "heading" >Contact Information</p><br /><br/>
		<table cellpadding = "7" >
		<tr><td><b class= "vpelements" >Phone No.  </b></td><td><?php echo $profile_data['phone_no'];?></td></tr>
		<tr><td><b class= "vpelements" >Email  </b></td><td><?php echo $profile_data['email'];?></td></tr>
		<tr><td><b class= "vpelements" >Web Page  </b></td><td><a href="<?php echo $profile_data['web_page']; ?>" class = "a" target = "_blank"><?php echo $profile_data['web_page'];?></a></td></tr>
		</table>
		<p class = "heading" >Activities &amp; Interests</p><br /><br/>
		<table cellpadding = "7" >
		<tr><td><b class= "vpelements" >Programming  </b></td><td><?php echo $profile_data['planguages'];?></td></tr>
		<tr><td><b class= "vpelements" >Expert In  </b></td><td><?php echo $profile_data['expert'];?></td></tr>
		<tr><td><b class= "vpelements" >Interested In  </b></td><td><?php echo $profile_data['interests'];?></td></tr>
		<tr><td><b class= "vpelements" >Like To Join   </b></td><td><?php echo $profile_data['work_for'];?></td></tr>
		<tr><td><b class= "vpelements" >After B.E.  </b></td><td><?php echo $profile_data['future_plans'];?></td></tr>
		</table>
	</td>
	</tr>
	</table>
</div>