<style>
	div.wrap{
	width: 135px;
	float:left; /* important */
	
	position:relative; /* important(so we can absolutely position the description div */
}
div.description{
	position:absolute; /* absolute position (so we can position it where we want)*/
	bottom:0px; /* position will be on bottom */
	left:0px;
	width:100%;
	/* styling bellow */
	padding-right:5px;
	background-color:black;
	font-family: 'tahoma';
	font-size:13px;
	color:white;
	opacity:0.6; /* transparency */
	filter:alpha(opacity=60); /* IE transparency */
}
p.description_content{
	padding:10px;
	margin:0px;
}

img.profile_pic{
	height: 135px;
	width: 135px;
	border: 3px solid #CCCCCC;
	-ms-object-fit: contain;
	-moz-object-fit: contain;
	-o-object-fit: contain;
	-webkit-object-fit: contain;
	object-fit: contain;
}
</style>
<div class = "linkcontent" style = "padding-top: 80px; width:900px;">
<?php
	
	$count = 0;
	$count2 = 1;
	
	echo '<table width="100%">';
	foreach($members_data as $member_data)
	{	
		if($count%4==0)
		{
			echo '<tr>';
		}
	
		$profile_pic = $member_data['thumb_profile_pic'];   
		if (is_file("profile_pics/" . $profile_pic) && filesize("profile_pics/" . $profile_pic) > 0)
		{
      		echo '<td style = "padding-bottom: 30px;" align="center">
      		<div class="wrap">
      		<a href=view/'.$member_data['user_id'].'>
      		<img src="' . PP_UPLOADPATH . $profile_pic . '" alt="' . htmlspecialchars($member_data['username']) . '" class = "profile_pic"/>
      		</a>
      		<div class="description">
      			<div class ="description_content">
      				<p style ="color:white;">'.htmlspecialchars($member_data['username']).'</p>
      			</div>
      		</div>
      		</div>
      		</td>';
    	}
    	else
     	{
      		echo '<td align="center">
      		<div class="wrap">
      		<a href=view/'.$member_data['user_id'].'>
      		<img src="' . PP_UPLOADPATH . 'nopic.jpg' . '" alt="' . $member_data['username'] . '" width="135" height="135" class = "profile_pic"/>
      		</a>
      		<div class = "description">
      			<div class = "description_content">
      				<p style ="color:white;">'.$member_data['username'].'</p>
      			</div>
      		</div>
      		</div>
      		</td>';
    		
		}
    	
      	
    	if($count2%4==0)
    	{
    		echo '</tr>';
    	}
       	
    	$count++;
    	$count2++;
	}
	echo '</table>';
?>
</div>