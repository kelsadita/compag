<div class = "linkcontent" style = "padding-top:20px;width:600px;">
<a href="<?php echo base_url();?>profile/view/<?php echo $video_user_id; ?>" class="hbutton" style = "float: right;">back</a>
<?php 


if($user_id != $video_user_id)
{
	$user_data = $this->login_model->get_user_info($video_user_id);
	$video_user_name = $user_data['username'];
	
	echo '<h2>'.$video_user_name.'\'s Albums</h2><br /><hr noshade style="border:2px solid #CCCCCC;"/>';
}
else 
{
	echo '<h2>My Videos</h2><br /><hr noshade style="border:2px solid #CCCCCC;"/>';
}

if ($this->session->flashdata('notice')) 
{
	echo '<p class = "notify">'.$this->session->flashdata('notice').'</p>';	
}


if(!empty($user_videos_info))
{
echo "<table width='100%'>";
foreach ($user_videos_info as $video_info) {

    echo "
    <tr>
        <td width='300' height='200' style='border-bottom:1px solid #DBDBDB;'>
        <iframe width='300' height='200' src='http://www.youtube.com/embed/".$video_info['url']."' frameborder='0' allowfullscreen></iframe>
        <br />
        <br />
        </td>
        <td style='border-bottom:1px solid #DBDBDB;'>
        <b style='color:#3b608a;'>".$video_info['title']."</b>";
        if($user_id == $video_user_id)
        echo "<a href='".base_url()."profile/delete_video/".$video_info['video_id']."/".$user_id."' style='float:right;'>Delete</a>";
        echo "
        </td>
            
    </tr>
    
    ";
    
}
echo "</table>";
}
else
{
    echo "<br /><br /><h2><center>No Videos found!</center></h2>";
}

?>
</div>
