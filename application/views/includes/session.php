<?php
	
	$this->load->helper('url');
	
	if(!($this->session->userdata('user_id'))){
		$url = urlencode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		if($title != 'Login' && $title != 'Forgot Password' && $title != 'Reset Password' && $title != 'Signup' && $title != 'Auth Login')
		{
			redirect('login/index/'.$url);
		}
	}
		
?>