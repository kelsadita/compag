<style>

table tr td{
	min-height : 800px;
}

a:hover{
	text-decoration : underline;
}
</style>
</head>
<div class = "linkcontent" style = "padding-top : 40px;">
<table width = "100%" style = "margin-top : 30px;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<a class = "dbutton" style = "float:right;" href = "<?php echo base_url();?>pings/write" >New Message</a>
		<h1 style = "color : #3b608a;">Messages</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br /><br />
		
		<?php
			
			foreach ($msgs_data as $msg_data) {
				
				$message = $msg_data['msg'];
				$sent_date_time = date('F j,Y h:i A', strtotime($msg_data['sent_date_time']));
				$msg_id = $msg_data['msg_id'];
				$sent_from = $msg_data['sent_from'];
			
				$userdata = $this->login_model->get_user_info($sent_from);
				
				$from_username = $userdata['username'];
				$profile_pic_name = $userdata['thumb_profile_pic'];

				$path = PP_UPLOADPATH.$profile_pic_name;
				
				echo '<table style = "border-bottom:1px solid #CCCCCC;width:600px; "><tr>';
				echo '<td style = "width:60px;"><image src = '.$path.' width = 55 height = 55></td>';
				echo '<td style = "font-size: 13px;"><a href = '.base_url().'profile/view/'.$sent_from.'>'.$from_username.'</a><b class = "fade" style = "float:right;">'.$sent_date_time
					  .'</b><br /><br /><b class = "fade">'.$message.'</b>';
				echo '</tr></table>';
			}
		?>
		
	</td>
	</tr>
</table>
</div>