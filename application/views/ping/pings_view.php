<div class = "linkcontent" style = "width: 800px; padding-top:40px;">
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<a class = "dbutton" style = "float:right;" href = "<?php echo base_url();?>pings/write" >New Message</a>
		<h1 style = "color : #3b608a;">Messages</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
		
		<?php 
			
			foreach ($pings_data as $ping_data) {
				
			
				$from_id = $ping_data['sent_from'];
				
				$ping_user_data = $this->ping_model->get_from_pings($from_id, $user_id);
				$count = $this->ping_model->get_msg_count($from_id, $user_id);
			
				$message = $ping_user_data['msg'];
				$sent_date_time = date('F j, Y',strtotime($ping_user_data['sent_date_time']));
				$msg_id = $ping_user_data['msg_id'];
				$notice = $ping_user_data['notice'];
				if(strlen($message)<50)
				{
					$msgstr = substr($message, 0, 50);
				}
				else
				{
					$msgstr = substr($message, 0, 50)."....";
				}
				$userdata = $this->login_model->get_user_info($from_id);
				$from_username = $userdata['username'];
				$profile_pic_name = $userdata['thumb_profile_pic'];
		
				$path = PP_UPLOADPATH.$profile_pic_name;
				echo '<a href = '.base_url().'pings/view/'.$from_id.'>';
			
				echo '<table style = "border-bottom:1px solid #CCCCCC;width:600px;';
				
				if($notice == 0)
				{
					echo 'background-color : #F1EDC2;"><tr>';
				}
				else 
				{
					echo '"><tr>';
				}
				echo '<td style = "width:60px;"><image src = '.$path.' width = 55 height = 55></td>';
				echo '<td style = "font-size: 13px;"><a href = '.base_url().'profile/view/'.$from_id.'>'.$from_username;
				if($count != 0)
				{
						echo '  <b style = "background-color : red; color : white; border-radius : 4px; padding : 2px;">'.$count.'</b>';
				}
				echo '</a><b class = "fade" style = "float:right;">'.$sent_date_time
					  .'</b><br /><br /><b class = "fade">'.$msgstr.'</b>';
				echo '</tr></table>';
				echo '</a>';
			
			}
		?>
		</td>
	</tr>
</table>

</div>