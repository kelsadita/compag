<link rel="stylesheet" href="<?php echo base_url();?>css/hovermenu.css" type="text/css" media="all" />
<script type="text/javascript">
	$(document).ready(function(){
                
                  $("#menub").click(function(){
                    
                        $("#menub").toggleClass("hoverb")
                        $("#submenus").toggle();
                    });     
            });
</script>
<div class = "linkcontent" >
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<h1 style = "color : #3b608a;"><?php echo $subject_data['sub_name']; ?></h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
		<?php 
			
			$userdata = $this->login_model->get_user_info($user_id);
			$status = $userdata['status'];
		
		?>
		

		<a class = "dbutton" href = "<?php echo base_url(); ?>semester"><?php echo "SEM".$sem_id;?> &gt;</a>
		<a class = "dbutton" href = "<?php echo base_url();?>semester/subject/<?php echo $sub_id; ?>"><?php echo $title;?> &gt;</a><br /><br />
		
		<?php 
			
			if ($status == "subadmin" || $status == "admin")
			{
		?>
				<div id = "menus" style="float: right">
				<div id = "button">
				<button id = "menub">Menus</button>
				</div>
				<div id = "submenus">
		    			<p class="menu"><a  style = float:right href ="<?php echo base_url(); ?>semester/updt_subject/<?php echo $sub_id; ?>" >Update This Page</a></p>
		    			<p class="menu"><a  style = float:right href = "<?php echo base_url(); ?>semester/delete_enteries/<?php echo $sub_id; ?>">Delete Entries</a></p>
				</div>
				</div>
		
		<?php		
				
			}
			
			$syllabus = $subject_data['syllabus'];
			$subject = $subject_data['sub_name'];
			
			if ($this->session->flashdata('notice')) {
				echo "<p class=notify>".$this->session->flashdata('notice')."</p>";
			}
			
			if(!empty($subject_data['description'])){
				echo "<p>".$subject_data['description']."</p>";
			}
			
			if(!empty($exps_data))
			{
				echo '<p class="heading">Experiments</p>';
			
				echo '<ol style = "margin-left:10px;">';
				foreach ($exps_data as $exp_data) {
				
					echo "<li><p class = a>".$exp_data['exp_title']."</p></li>";
					echo $exp_data['exp_des'].'<hr noshade style = "border: 1px solid #CCCCCC">';
				}
				echo '</ol>';
			}
		
			if(!empty($asgns_data))
			{
				echo '<p class="heading">Assignments & Tutorials</p>';
		
				foreach ($asgns_data as $asgn_data) {
					
					echo '<table cellpadding = "5" width="100%" style  = "border-bottom: 1px solid #CCCCCC;"><tr>';
					echo "<td ><p class = a>".$asgn_data['assgn_title']."</p>";
					echo "<p class = fade>Dated : ". date('F j,Y h:i A', strtotime($asgn_data['submission_date']))."</p>";
					echo $asgn_data['assgn_des']."</td>";
					echo '</tr></table>';
				}	
			}
			
			echo '<p class="heading">Recommanded Reference Books</p>';
		
			echo '<ol>';
			foreach ($books_data as $book_data) {
			
				$book = $book_data['book_name'];
				$url = $book_data['url'];
				
				$author = $book_data['author'];
				$publication = $book_data['publication'];
				echo '<li><a class = "a" href = "'.$url.'">'.$book.'</a></li><br />';
				echo "<b class = fade>Author </b>: ".$author." <b class = fade style = margin-left:40px;>Publication : </b>".$publication."<hr />";
			}
			echo '</ol>';
				
			
		?>

	</td>
	</tr>
</table>

</div>