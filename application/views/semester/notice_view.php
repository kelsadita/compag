<div class = "linkcontent" style = "width: 800px;">
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Notice Board</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
		
		<a class = "dbutton" href = "<?php echo base_url();?>semester"><?php echo "SEM".$sem_id;?> &gt;</a>
		<a class = "dbutton" href = "<?php echo base_url();?>semester/notice">Notifications &gt;</a><br /><br />
		<?php 
			
			$userdata = $this->login_model->get_user_info($user_id);
			$status = $userdata['status'];
		
			if($status == "admin" || $status == "subadmin")
			{
				echo "<a class= hbutton style = float:right href=".base_url()."semester/add_notice/".$sem_id.">Add Notice</a><br /><br />";
			}
			
			echo '<p class="heading">Recent Notices</p>';
			
			if($this->session->flashdata('notice'))
			{
				echo '<p class="notify">'.$this->session->flashdata('notice').'</p>';
			}
		
			foreach ($notices_data as $notice_data) {
				
				$notice_title = $notice_data['notice_title'];
				$notice_des = $notice_data['notice_des'];
				$add_datetime = date('F j,Y h:i A', strtotime($notice_data['n_add_datetime']));
				$nid = $notice_data['nid'];
				
				if($status == "admin" || $status == "subadmin")
				{
					echo "<a class= dbutton style = float:right href=".base_url()."semester/delete_notice/".$nid.">delete</a>";
				}
				
				echo '<div style="margin-left: 10px;"><b class = "a">'.$notice_title.'</b><br />';
				echo '<b class = "fade">Dated : '.$add_datetime.'</b>';
				echo '<p>'.$notice_des.'</p><hr noshade style="border: 2px solid #CCCCCC;"/></div>';
			}
			
			
			
		?>	
	</td>
	</tr>
</table>

</div>