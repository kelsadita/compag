<script type = "text/javascript">
	$(function() 
	{
		$( "#accordion" ).accordion();
	});
</script>
<div class = "linkcontent" style = "width: 800px;">
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Semester</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
			<a class = "dbutton" href = "<?php echo base_url(); ?>semester/update/<?php echo $user_id; ?>">Update Sem</a>

			<div id = "accordion" style = "padding:50px 0px 50px 0px;">
			<h2><a href = "#">Notice Board</a></h2>
			<div>
			<?php 
	
				foreach ($notice_data as $notice) {
		
	
				$notice_title = $notice['notice_title'];
				$notice_des = $notice['notice_des'];
				$add_date = date('F j,Y h:i A', strtotime($notice['n_add_datetime']));
		
				echo '<b class = "a">'.$notice_title.'</b><br />';
				echo '<b class = "fade">Dated : '.$add_date.'</b>';
				echo '<p>'.$notice_des.'</p><hr />';
				break;
				}
			?>
			<a class = "hbutton" href = "<?php echo base_url(); ?>semester/notice">See All</a>
			</div>
			<h2><a href = "#">Archives</a></h2>
			<div>
				<ul class = "archives">
					<li><a href = "semester/timetable">Time Table</a></li>
					<li><a href = "semester/tests">Class Exam Schedule</a></li>
					<li><a href = "semester/notice">Notifications</a></li>
					<li><a href = "semester/files/<?php echo $sem_id ; ?>">Downloads</a></li>
				</ul>
			</div>

			<h2><a href = "#">Subjects</a></h2>
			<div>
			<?php 
					
					
					echo '<ul class = "subjects">';
					foreach ($subjects_data as $subject_data) {
						
						$sub_name = $subject_data['sub_name'];
						$sub_id = $subject_data['sub_id'];
						echo "<li><a href=".base_url()."semester/subject/".$sub_id.">".$sub_name."</a></li>";
					}
					echo '</ul>';
			
			?>
			</div>
			</div>
		</td>
	</tr>
</table>

</div>