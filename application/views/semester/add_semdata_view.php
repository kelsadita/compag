<script type="text/javascript" src="<?php echo base_url();?>scripts/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>
<script>
  $(document).ready(function() {
    $("#submission_date").datepicker({dateFormat: 'yy-mm-dd'});
    
    $("#category").change(function(){
    	var selection = $(this).val();
    	if(selection == 'experiments')
    	{
    		$("#sdate").fadeOut();
    		$("#submission_date").removeClass('required');
    	}
    	else if(selection == 'description')
    	{
    		$("#sdate,#titled").fadeOut();
    		$("#submission_date,#title").removeClass('required');
    	}
    	else
    	{
    		$("#sdate,#titled").fadeIn();
    		$("#submission_date,#title").addClass('required');
    	}
    	
    });
    
    $("#adddata2").validate();
  });
</script>

</head>
<body>
<?php 
	
	$userdata = $this->login_model->get_user_info($user_id);
	
	if($userdata['status'] == "member")
	{
		redirect(base_url().'semster');
	}

?>
<div class = "linkcontent" style = "width: 700px; padding-top:20px; padding-bottom:30px;">
<a class = "dbutton" href = "<?php echo base_url(); ?>semester"><?php echo "SEM".$sem_id;?> &gt;</a>
<a class = "dbutton" href = "<?php echo base_url();?>semester/subject/<?php echo $sub_id; ?>"><?php echo $title;?> &gt;</a>
<a class = "dbutton" href = "<?php echo base_url();?>semester/updt_subject/<?php echo $sub_id; ?>">Admin page</a><br /><br />
<h3>Update The Subject Page</h3>
<form action="<?php echo base_url(); ?>semester/updt_subject/<?php echo $sub_id; ?>" method="post" id="adddata">
	<table>
	<tr><td>
	<label for = "category">Category : </label>
	</td>
	<td>
	<select style = "height: 25px;" id ="category" name ="category">
	    <option>assignments and tutorials</option>
	    <option>experiments</option>
	    <option>description</option>
	</select><br /><br />
	</td></tr>
	<tr id="sdate"><td>
	<label for = "submission_date" >Due Date : </label>
	</td>
	<td>
	<input type = "text" class = "required" id = "submission_date" name = "submission_date" style = "width:300px;"/><br />
	</td></tr>
	<tr id="titled"><td>
	<label for = "title">Title : </label>
	</td>
	<td>
	<input type = "text" class = "required" id = "title" name = "title"  style = "width:300px;"/><br /><br />
	</td></tr>
	<tr><td></td>
	<td>
	<textarea name="description" rows="10" cols="60"></textarea>
	</td></tr>
	<tr><td></td>
	<td>
	<input type="submit" class = "button" style = "width: 70px;" value="submit" name = "submit"/><br />
	</td></tr>
	</table>
</form>
<p style = "margin-left:100px;">Note : please enter the submission date for the assignments</p>
<h3>For Reference Books</h3>
<form action="<?php echo base_url(); ?>semester/updt_subject/<?php echo $sub_id; ?>" method = "post" id="adddata2">
	<label for = "book_name">Book Name* : </label>
	<input type = "text" id = "book_name" name = "book_name" class = "required"/><br />
	<label for = "author">Author* : </label>
	<input type = "text" id = "author" name = "author" class = "required"/><br />
	<label for = "publication">Publication : </label>
	<input type = "text" id = "publication" name = "publication" /><br />
	<label for = "url">Reference URL : </label>
	<input type = "text" id= "url" name = "url" /><br />
	<label></label>
	<input type = "submit" id = "bsubmit" name = "bsubmit" value = "submit" class = "button"/>
</form>
</div>