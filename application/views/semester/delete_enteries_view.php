<style>
	#book,#assignment,#experiment{
		border-collapse: collapse;
		border: 1px solid #CCCCCC;
	}
</style>
<div class = "linkcontent" style = "padding-top:20px;">

<a  class = "dbutton" href = "<?php echo base_url()."semester";?>"><?php echo "SEM".$sem_id;?> &gt;</a>
<a  class = "dbutton" href = "<?php echo base_url();?>semester/subject/<?php echo $sub_id;?>"><?php echo $title;?> &gt;</a>
<a  class = "dbutton" href = "<?php echo base_url(); ?>semester/delete_enteries<?php echo $sub_id;?>">Admin page</a><br /><br />
<?php 
	
	$userdata = $this->login_model->get_user_info($user_id);
	$status = $userdata['status'];

	if($status == "admin" || $status == "subadmin")
	{
		if ($this->session->flashdata('notice')) {
		echo "<p class=notify>".$this->session->flashdata('notice')."</p>";
		}
		show_entries($sub_id,$sem_id,$exps_data,$asgns_data,$books_data);
	}
	else 
	{
		redirect(base_url()."semester");
	}
	
	function show_entries($sub_id,$sem_id,$exps_data,$asgns_data,$books_data)
	{
		
		echo '<h3>Experiments</h3>';
		echo '<table id ="experiment" border = "1" cellpadding = "10" width="100%">';
		echo '<tr><th>Title</th><th>Description</th><th>Action</th></tr>';
		foreach ($exps_data as $exp_data) {
		
			$exp_title = $exp_data['exp_title'];
			$exp_des = $exp_data['exp_des'];
			$exp_id = $exp_data['exp_id'];
			
			echo "<tr><td>".$exp_title."</td>";
			echo "<td>".$exp_des."</td>";
			echo '<td><a class = dbutton href="'.base_url().'semester/delete_experiment/'.$sub_id.'/'.$exp_id.'">Remove</a></td></tr>';
		}
		echo '</table>';
		
		echo '<h3>Assignments/Tutorials</h3>';
		echo '<table id="assignment" border = "1" cellpadding = "10" width="100%">';
		echo '<tr><th>Title</th><th>Description</th><th>Added On</th><th>Submission Date</th><th>Action</th></tr>';
		foreach ($asgns_data as $asgn_data) {
				
			$assgn_title = $asgn_data['assgn_title'];
			$assgn_name = $asgn_data['assgn_des'];
			$submission_date = $asgn_data['submission_date'];
			$assgn_add_date = $asgn_data['assgn_add_date'];
			$assgn_id = $asgn_data['assgn_id'];
			
			echo "<tr><td>".$assgn_title."</td>";
			echo "<td>".$assgn_name."</td>";
			echo "<td>".$assgn_add_date."</td>";
			echo "<td>".$submission_date."</td>";
			echo '<td><a class = dbutton href="'.base_url().'semester/delete_assignment/'.$sub_id.'/'.$assgn_id.'">Remove</a></td></tr>';
		}
		echo '</table>';
		
		echo '<h3>Reference books</h3>';
		echo '<table id = "book" border = "1" cellpadding = "10" width="100%">';
		echo '<tr><th>Name</th><th>Author</th><th>Publication</th><th>Action</th>';
		foreach ($books_data as $book_data) {
	
			$book_name = $book_data['book_name'];
			$author = $book_data['author'];
			$publication = $book_data['publication'];
			$book_id = $book_data['book_id'];
			
			echo "<tr><td>".$book_name."</td>";
			echo "<td>".$author."</td>";
			echo "<td>".$publication."</td>";
			echo '<td><a class = dbutton href="'.base_url().'semester/delete_refbook/'.$sub_id.'/'.$book_id.'">Remove</a></td></tr>';
		}
		echo '</table>';
		
		
	}
	
	
?>
</div>