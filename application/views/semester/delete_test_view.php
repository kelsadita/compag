<?php include './includes/doctype.inc.php';?>
<title>Admin | compag</title>
<link rel="stylesheet" href="style.css" type="text/css" media="all" />
<link rel="stylesheet" href="linkstyle.css" type="text/css" media="all" />
</head>
<body>
<?php  include './includes/header.inc.php';?>
<div class = "linkcontent" style = "padding-top:20px;">
<a class = "dbutton" href = "<?php echo "class.php?sem_id=".$_GET['sem_id'];?>"><?php echo "SEM".$_GET['sem_id'];?> &gt;</a>
<a class = "dbutton" href = "<?php echo "examschedule.php?sem_id=".$_GET['sem_id'];?>">Exam Schedule &gt;</a>
<a class = "dbutton" href = "<?php echo "manage_exams.php?sem_id=".$_GET['sem_id'];?>">Admin Page &gt;</a><br /><br />
<?php 
	require_once './includes/connect.inc.php';
	
	$status = $_SESSION['status'];
	
	$db = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	
	if (isset($_GET['sem_id']))
	{
		$sem_id = $_GET['sem_id'];
	}
	
	$query = "select distinct test_cat from exam_schd where sem_id = $sem_id";
	$data = mysqli_query($db,$query);
	
	while($row = mysqli_fetch_array($data))
	{
		$category = $row['test_cat']; 
		
		echo '<h3 style = "margin-top:30px;">'.$category.'</h3>';
		$test_query = "select * from exam_schd where test_cat = '$category' and sem_id=$sem_id";
		$test_data = mysqli_query($db,$test_query);
		
		echo '<center><table border = "1" cellpadding = "10" style = "margin-bottom:20px;">';
		echo '<tr ><th>Subject</th><th>Date</th><th>Syllabus</th><th>Action</th></tr>' ;
		while ($test_row = mysqli_fetch_array($test_data))
		{
			$subject = $test_row['subject'];
			$date = $test_row['tdate'];
			$syllabus = $test_row['syllabus'];
			$test_id = $test_row['test_id'];
			
			echo '<tr><td>'.$subject.'</td>';
			echo '<td>'.$date.'</td>';
			echo '<td>'.$syllabus.'</td>';
			echo "<td><a class = dbutton href = sub_remove.php?sem_id=$sem_id&amp;test_id=$test_id >Delete</a></td></tr>";
		}
		echo '</table></center>';
	}
	
	mysqli_close($db);

?>
</div>
<?php include './includes/footer.inc.php';?>