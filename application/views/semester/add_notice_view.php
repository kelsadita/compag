<script type="text/javascript" src="<?php echo base_url();?>scripts/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>
<div class = "linkcontent" style = "width: 800px;">
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Add Notice</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
		<a class = "dbutton" href = "<?php echo base_url();?>semester"><?php echo "SEM".$sem_id;?> &gt;</a>
		<a class = "dbutton" href = "<?php echo base_url()."semester/notice"?>">Notifications&gt;</a>
		<a class = "dbutton" href = "<?php echo base_url()."semester/add_notice/".$sem_id;?>">Admin page</a><br /><br />
		
		<form action="<?php echo base_url();?>semester/add_notice/<?php echo $sem_id; ?>" method="post" id="adddata">
			
			<table style = "margin:0 auto;" cellpadding = "8">
			<tr>
			<td class = "fade" >Title</td>
			<td><input type = "text" id = "notice_title" name = "notice_title" class="required"/></td>
			</tr>
			<tr>
			<td class = "fade" >Notice</td>	
			<td><textarea rows="10" cols="60" name="notice"></textarea></td>
			</tr>
			<tr>
			<td></td>
			<td><input type="submit" class = "button" style = "width: 70px;" value="submit" name = "submit"/></td>
			</tr>
			</table>
		</form>	
	</td>
	</tr>
</table>

</div>