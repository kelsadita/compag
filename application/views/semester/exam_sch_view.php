<script type="text/javascript">
	$(document).ready(function(){
		$("#tdate").datepicker({dateFormat: 'yy-mm-dd'});
	});
</script>

<style type="text/css">
	#t{
		border-collapse: collapse;
		border: 1px solid #CCCCCC;
		width: 500px;
	}
</style>
<div class = "linkcontent" style = "width: 800px;">
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Test Schedule</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
			<a class = "dbutton" href = "<?php echo base_url();?>semester"><?php echo "SEM".$sem_id;?> &gt;</a>
			<a class = "dbutton" href = "<?php echo base_url()."semester/tests";?>">Exam Schedule &gt;</a><br /><br />
			<?php 
				
				$userdata = $this->login_model->get_user_info($user_id);
				$status = $userdata['status'];
			
				if($status == "admin" || $status == "subadmin")
				{
					echo "<button style = float:right; >Add Exam</button><br /><br />";
				}
			
				if ($this->session->flashdata('notice')) {
					echo '<p class="notify">'.$this->session->flashdata('notice').'</p>';
				}
			?>
			<div id = "add" style = "display:none">
			<p class="heading">Add Exams</p>
			<form action="<?php echo base_url();?>semester/add_test/<?php echo $sem_id; ?>" method = "post" id = "adddata">
			<label for = "subject">Subject : </label>
			<select name = "subject" style = "margin:0px 0px 10px 0px;">
			
			<?php
				
				foreach ($subjects_data as $subject_data) {
					
					echo '<option>'.$subject_data['sub_name'].'</option>';
				}
			
			?>
			</select><br />
			<label for = "test_cat">Category : </label>
			<select name = "test_cat" style = "margin:0px 0px 10px 0px;">
			<option>Test1</option>
			<option>Test2</option>
			<option>Test3</option>
			</select><br />
			<label for = "tdate">Date : </label>
			<input class = "required" type = "text" id = "tdate" name = "tdate" /><br />
			<label for="syllabus" style = "vertical-align:top;">Syllabus : </label>
			<textarea class = "required" name = "syllabus" rows="7" cols="50" style = "margin:0px 0px 10px 0px;"></textarea><br />
			<label>&nbsp;</label>
			<input type = "submit" name = "submit" class = "button" value = "submit">
			</form>
			<p class = "fade" style = "margin-left: 110px;">NOTE : please enter all the fields</p>
			</div>
			<?php 
					
			
				foreach ($test_cat as $category) {
				
					$category = $category['test_cat']; 
					
					echo '<p class="heading">'.$category.'</p>';
					
					$tests_data = $this->sem_model->get_test_data($sem_id, $category);
						
					echo '<center><table id="t" border = "1" cellpadding = "10" style = "margin-bottom:20px;">';
					echo '<tr ><th>Subject</th><th>Date</th><th>Syllabus</th><th>Action</th></tr>' ;
					foreach ($tests_data as $test_data) {
						
					
						$subject = $test_data['subject'];
						$date = $test_data['tdate'];
						$syllabus = $test_data['syllabus'];
						$test_id = $test_data['test_id'];
						
						echo '<tr><td>'.$subject.'</td>';
						echo '<td>'.$date.'</td>';
						echo '<td>'.$syllabus.'</td>';
						if($status == "admin" || $status == "subadmin")
						{
							echo '<td><a class ="dbutton" href="'.base_url().'semester/delete_test/'.$test_id.'">Delete</a></td>';
						}
						
						
						echo '</tr>';
					}
					echo '</table></center>';
				}
			
			?>
		</td>
	</tr>
</table>

</div>