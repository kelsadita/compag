<div class = "linkcontent" style = "width: 800px;">
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<h1 style = "color : #3b608a;">Upload A File</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
		<a class = "dbutton" href = "<?php echo base_url()."semester";?>"><?php echo "SEM".$sem_id;?> &gt;</a>
		<a class = "dbutton" href = "<?php echo base_url();?>semester/files/<?php echo $sem_id; ?>">File Sharing</a><br /><br /><br />

		<?php
				
			if ($this->session->flashdata('notice')) {
				echo "<p class=notify>".$this->session->flashdata('notice')."</p>";
			}
		
		?> 
		<form id = "adddata" action="<?php echo base_url(); ?>semester/upload_file/<?php echo $sem_id; ?>" method="post" enctype="multipart/form-data" > 
			<label for = "title">Filename* : </label>
			<input type = "text" name = "title" id = "title" class = "required" /><br />
			<label for  = "category">Category : </label>
			<select style = "height: 25px; margin-bottom: 5px;" id ="category" name ="category">
		    	<?php 
		    		foreach ($subject_data as $subject) {
						
		    			echo "<option>".$subject['sub_name']."</option>";
		    		}
		    	?>
		    	<option>Softwares</option>
		    	<option>Others</option>
			</select><br />
			<label for = "description">Description : </label>
			<textarea name = "description" style = "vertical-align:top; width:200px;"></textarea></br>
			<label for = "file">File link* : </label>
			<input type = "text" name = "link" id = "link" class = "required" /><br />
			<label>&nbsp;</label>
			<input type ="submit" name = "submit" value = "upload" class = "button"/><br/>	
			<p style="margin-left:120px">Please add sharing link from <b>box.com</b></p>
		</form>

		<p class="heading">All Files</p>
		<?php
	
		    	
		    	echo "<ul style = list-style-image:url('".IMAGES_BASE."/folder.png');>";
		    	foreach ($subject_data as $subject) {
						
					$hsubject = str_replace(" ", "-", $subject['sub_name']);
		    		echo "<li><a class = a href=".base_url()."semester/view_files/$hsubject/$sem_id>".$subject['sub_name']."</a></li><br />";
		    	}
		    	echo "<li><a class = a href=".base_url()."semester/view_files/Softwares/$sem_id>Softwares</a></li><br />";
		    	echo "<li><a class = a href=".base_url()."semester/view_files/Others/$sem_id>Others</a></li><br />";
		    	echo "</ul>"
		?>
	
		
	</td>
	</tr>
</table>

</div>