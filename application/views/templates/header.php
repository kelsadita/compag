<?php 
	$this->load->view('includes/session');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">

<meta http-equiv="X-UA-Compatible" content="IE=Edge" />


<title><?php echo $title; ?> | compag</title>

<link rel="shortcut icon" href="<?php echo base_url();?>images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="<?php echo base_url();?>css/android.css"  type="text/css" media="only screen and (max-width: 480px)" />
<link rel="stylesheet" href="<?php echo base_url();?>css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo base_url();?>css/linkstyle.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo base_url();?>css/colorbox.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo base_url();?>css/jqueryui.css" type="text/css" media="all" />


<!--Scripts-->
<script src="<?php echo base_url();?>scripts/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>scripts/jquery.validate.js"></script>
<script src="<?php echo base_url();?>scripts/jqueryui.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>scripts/jquery.colorbox.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>scripts/jquery.ba-dotimeout.js" type="text/javascript"></script>


<script>
 
  $(document).ready(function(){
  	
  	
  	
	$("button").click(function(){
		$("#add").toggle(1000,function(){
			var display = document.getElementById("add").style.display;
			
			if(display != "none")
			{
				$(".updt_table").fadeOut("fast");
			}
			else
			{
				$(".updt_table").fadeIn();
			}	
		});
		
		
			
	});
	
	// validate
	$("#adddata").validate();

	
	var clientWidth = $(document).width(); 
	$("#header, .band").css("width", clientWidth);
	});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28380729-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>

<div id="header" >
<div style ="width: 900px; margin: 0 auto;">


<span>
<a href="http://www.compag.in/home">
<image src="<?php echo IMAGES_BASE;?>compag.png" style="float: left; height : 45px; padding-top: 20px;"/>
</a>
</span>

<span style = "width : 100%">
<?php 
	
if ($title == 'Login' || $title == 'Auth Login' ) { 
	
	if($title == 'Auth Login')
	{
	
		$pageURL = $_SERVER["REQUEST_URI"];
		$raw_url = explode("/", $pageURL);
		$hash = $raw_url[3];

?>
		<script type = "text/javascript">
			$(document).ready(function(){
				
				
				setInterval(function(){
					$.get('<?php echo base_url(); ?>login/check/<?php echo $hash; ?>', function(data){ if(data.result == 1){window.location.replace("http://www.compag.in/home");}
					 }, "json");      
    				}, 1000);

			});
		</script>
<?php
	}
?>	
	<div style = "float : right; margin : 25px 10px 0px 0px; ">	
    <b style = "color:white; font-weight: bold; font-family: monaco,consolas; font-size: 18px;">Not a member yet! &nbsp;&nbsp;</b><a href ="<?php echo base_url();?>login/register"><button  class ="rbutton">join us</button></a>
	</div>
<?php } 
else if($title == 'Forgot Password' || $title == 'Reset Password' || $title == 'Signup'){
	echo '<div style = "float : right; margin : 25px 10px 0px 0px; ">	
    		<a href ="http://www.compag.in" class="dbutton">back</a>
		</div>';
	
}
else{ ?>
<div style = "float : right; margin : 10px 10px 0px 0px;">	
	<ul class="navbar">
		<li><a href="<?php echo base_url();?>home" <?php if($title=='Home') echo 'id="here"';?>>Home</a></li>
		<!--<li><a href="<?php echo base_url();?>awards" <?php if($title=='Awards') echo 'id="here"';?>>Awards</a></li>-->
		<li><a href="<?php echo base_url();?>photos" <?php if($title=='Photos') echo 'id="here"';?>>Photos</a></li>
		<li><a href="<?php echo base_url();?>links" <?php if($title=='Links') echo 'id="here"';?>>Links</a></li>
		<li><a href="<?php echo base_url();?>videos" <?php if($title=='Videos') echo 'id="here"';?>>Videos</a></li>
		<!--<li><a href="http://localhost/n-pac1/polls.php" <?php if($title=='polls.php') echo 'id="here"';?>>Polls</a></li>-->
		<li><a href="<?php echo base_url(); ?>discuss/most_recent" <?php if($title=='Discuss') echo 'id="here"';?>>Discuss</a></li>

		<li><a href="<?php echo base_url();?>codes" <?php if($title=='Codes') echo 'id="here"';?>>Codes</a></li>
	</ul>
</div>
<?php
}
?>
</span>
</div>
</div>
<div class ="band">
</div>