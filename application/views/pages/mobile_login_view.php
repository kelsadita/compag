<!DOCTYPE html> 
<html> 
    <head> 
    <title>My Page</title> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery-mobile.css" type="text/css" media="all" />
    
	<script type="text/javascript" src="<?php echo base_url();?>scripts/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>scripts/jquery-mobile.js"></script>
	<style type="text/css">
		.notify
		{
			padding-top:7px;
			text-align:center;
			font-weight:bold; 
			color:#8B5742;
			background-color:#EEEED1; 
			height:22px;
			border-radius:15px;
			-moz-border-radius:15px;
		}
	</style>
</head> 
<body> 

<div data-role="page" id="login">

  <div data-role="header">
    <h1>[compag]</h1>
        
  </div><!-- /header -->

  <div data-role="content">
  	
  	<?php
  		$form_visible = 1;
  		if(!empty($notice))
  		{
  			if($notice == 'success')
  			{
  				echo "<p class=notify>Login sucessful, now refresh the page on your Desktop</p>";
  				$form_visible = 0;
  			}
  			else
  			{
  				echo "<p class=notify>".$notice."</p>";
  			}
  		}

  		
  	if($form_visible == 1)
  	{	
  		
  	?>
  	
	        <form action="<?php echo base_url(); ?>login/mlogin/<?php echo $key; ?>" method="post">
	        <div data-role="fieldcontain" class="ui-hide-label">
	            <label for="email">Email:</label>
	            <input type="text" name="email" id="email" value="" placeholder="Email"/><br />
	            <label for="password">Password:</label>
	            <input type="password" name="password" id="password" value="" placeholder="Password"/><br />
	            <input type="submit" value="submit" name="submit"/>
	        </div>
	        </form>
        <?php
        }
        else
        {
        ?>
        	<center><br /><br /><br /><img src = "http://www.compag.in/images/refresh.jpg" /></center>
        <?php
        }
        ?>
   
        	
  </div><!-- /content -->
    
</div><!-- /page -->

</body>
</html>