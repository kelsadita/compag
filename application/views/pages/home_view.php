<div class="linkcontent" >
<table width = "100%" style = "margin-top : 30px; border-right: 1px solid #DEDEDE;">
	<tr>
	<?php $this->load->view('includes/sidebar') ?>
	<td width = "80%"  class = "format">
		<h1 style = "color : #3b608a;">Recent feeds</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
		<?php 
              
				foreach ($feeds_data as $feed_data) {
					
					$title_id = $feed_data['title_id'];
					if($feed_data['updt_category'] == 'link')
					{
						$link_data = $this->links_model->get_link($title_id);
					
						$user_id = $link_data['user_id'];
						$title = $link_data['title'];
						$url = $link_data['url'];
						$description = $link_data['description'];
						$category = $link_data['category'];
						$added_on = date('F j,Y h:i A', strtotime($link_data['add_date_time']));
						
						$userdata = $this->login_model->get_user_info($user_id);
						
						$username = $userdata['username'];
						$thumb_profile_pic = $userdata['thumb_profile_pic'];
						$path = PP_UPLOADPATH.$userdata['thumb_profile_pic'];
						echo '<table class = "updt_table"><tr>';
						echo "<td style = width:60px;><image src = $path width = 55 height = 55></td>";
						echo "<td><a href = ".base_url()."profile/view/$user_id>$username</a> added a link for <b><a class = a href = $url target = _blank>$title</a>
						</b> in $category<br /><p class = fade style = text-align:justify;>$description</p><p class=fade>".$added_on."</p></td>";
						echo '</tr></table>';
					}
					elseif ($feed_data['updt_category'] == 'discuss')
					{
						$discuss_data = $this->discuss_model->get_topic($title_id);
						
						$user_id = $discuss_data['user_id'];
						$topic = $discuss_data['topic'];
						$did = $discuss_data['did'];
						
						$userdata = $this->login_model->get_user_info($user_id);
						
						$username = $userdata['username'];
						$thumb_profile_pic = $userdata['thumb_profile_pic'];
						$path = PP_UPLOADPATH.$userdata['thumb_profile_pic'];
						echo '<table class = "updt_table"><tr>';
						echo "<td width = '60'><image src = $path width = 55 height = 55></td>";
						echo '<td><a href = '.base_url().'profile/view/'.$user_id.'>'.$username.'</a> added a topic  <b>
						<a class = a href = '.base_url().'discuss/view/'.$did.'>'.$topic.'</a></b><br /><br />
						<b class = "fade">'.date('F j,Y h:i A', strtotime($discuss_data['add_date_time'])).'</b>
						<b style = "padding-left:50px;color:#A9A9A9;">Comments : </b>'.$discuss_data['num_ans'].'</td>';
						echo '</tr></table>';
					}
					
					elseif ($feed_data['updt_category'] == 'codes')
					{
						$codes_data = $this->codes_model->get_code($title_id);
						
						$user_id = $codes_data['user_id'];
						
						$code_id = $codes_data['code_id'];
						$code_title = $codes_data['code_title'];
						$code_category = $codes_data['code_category'];
						$code_add_date = date('F j,Y h:i A', strtotime($codes_data['code_add_datetime']));
					
						$userdata = $this->login_model->get_user_info($user_id);
						
						$username = $userdata['username'];
						$profile_pic = $userdata['thumb_profile_pic'];
						$path = PP_UPLOADPATH.$userdata['thumb_profile_pic'];
						echo '<table class = "updt_table"><tr>';
						echo "<td style = width:60px;><image src = $path width = 55 height = 55></td>";
						echo "<td><a href = ".base_url()."profile/view/$user_id>$username</a> added a code for <b>
						<a class = a href = ".base_url()."codes/view/$code_id >$code_title</a></b><br/><br/>
						<b class = fade>category : $code_category </b><p class=fade>".$code_add_date."</p></td>";
						echo '</tr></table>';
					}
					elseif ($feed_data['updt_category'] == 'albums')
					{
						$album_data = $this->photos_model->get_album_info($title_id);
						$user_id = $album_data['user_id'];
						
						$album_id = $album_data['album_id'];
						$album_add_date = $album_data['album_add_datetime'];
						$album_user_id = $album_data['user_id'];
						$name = $album_data['name'];

						$userdata = $this->login_model->get_user_info($user_id);
						
						$username = $userdata['username'];
						$profile_pic = $userdata['thumb_profile_pic'];
						$path = PP_UPLOADPATH.$userdata['thumb_profile_pic'];					
						echo '<table class = "updt_table"><tr>';
						echo '<td style = "width:60px;"><image src = '.$path.' width = 55 height = 55></td>';
						echo '<td style = "font-size: 13px;">
						<a href = '.base_url().'profile/view/'.$album_user_id.'>'.$username.'</a>
						added album <b><a class = "a" href = '.base_url().'photos/view/'.$album_id.'>'.$name.'</a>
						<br /><br /><b class = "fade">'.date('F j,Y h:i A', strtotime($album_data['album_add_datetime']))."</b>";
						echo '</tr></table>';
					}
					elseif ($feed_data['updt_category'] == 'fshare')
					{
						$filedata = $this->sem_model->get_file_info($title_id);
						$user_id = $filedata['user_id'];
						
						$fid = $filedata['fid'];
						$added_on = $filedata['added_on'];
						$filelink = $filedata['filelink'];
						$category = $filedata['category'];
						$description = $filedata['description'];
						
						$userdata = $this->login_model->get_user_info($user_id);
						
						$username = $userdata['username'];
						$profile_pic = $userdata['thumb_profile_pic'];
						$path = PP_UPLOADPATH.$userdata['thumb_profile_pic'];
						
						echo '<table class = "updt_table"><tr>';
						echo '<td style = "width:60px;"><image src = '.$path.' width = 55 height = 55></td>';
						echo '<td style = "font-size: 13px;">
						<a href = '.base_url().'profile/view/'.$user_id.'>'.$username.'</a>
						added a <b><a class = "a" href = '.$filelink.'>file</a></b> in <em>'.$category.'</em>
						<br /><br /><p class = fade style = text-align:justify;>'.$description.'</p><b class = "fade">'.date('F j,Y h:i A', strtotime($added_on))."</b>";
						echo '</tr></table>';	
					}
					else
					{
						$video_data = $this->videos_model->get_video($title_id);
						$user_id = $video_data['user_id'];
						
						$video_id = $video_data['video_id'];
						$video_add_date = $video_data['video_add_datetime'];
						$title = $video_data['title'];
						$url = $video_data['url'];
						
						$userdata = $this->login_model->get_user_info($user_id);
						
						$username = $userdata['username'];
						$profile_pic = $userdata['thumb_profile_pic'];
						$path = PP_UPLOADPATH.$userdata['thumb_profile_pic'];		
						
						echo '<table class = "updt_table"><tr>';
						echo '<td style = "width:60px;"><image src = '.$path.' width = 55 height = 55></td>';
						echo '<td style = "font-size: 13px;"><a href = '.base_url().'profile/view/'.$user_id.'>'.$username.'</a> added video <b class = "a">'.$title.'</b><br /><br />';
						echo "<iframe width=300 height=200 src=http://www.youtube.com/embed/$url frameborder=0 allowfullscreen></iframe>
						<br /><p class=fade>".date('F j,Y h:i A', strtotime($video_add_date))."</p><br />";
						//echo '<div class="fb-like" data-href="http://localhost/n-pac1/home.php" data-send="true" data-width="450" data-show-faces="true"></div>';
						echo '</tr></table>';
						
						
						
					}

					
				}

				if(strlen($pagination))
				{
					echo '<div class = "pagination"><center>'.$pagination.'</center></div>';	
				}
                               
		?>			
	</td>
	</tr>
</table>	
</div>