<script>

  $(document).ready(function(){
	
	$("#birthday").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
    	changeYear: true,
    	yearRange: '1985:2000'
		});
	});
</script>

<div class = "linkcontent" style = "padding-top:50px;">

<h2>Sign Up</h2><hr />
<?php
	if($this->session->flashdata('notice'))
	{
		echo '<p class="notify">'.$this->session->flashdata('notice').'</p>';
	}
?>
<form id = "adddata" enctype = "multipart/form-data" method ="post" action ="" >
	<label for ="firstname">First Name:</label>
	<input class = "required" id ="firstname" name ="firstname" type ="text" /><br />
	<label for ="lastname">Last Name:</label>
	<input id ="lastname" name ="lastname" type ="text" /><br />
	<label for = "password1">Password:</label>
	<input class = "required" id ="password1" name ="password1" type ="password"/><br />
	<label for = "password2">Repeat Password:</label>
	<input class = "required" id ="password2" name ="password2" type ="password"/><br />
	<label for ="email">Email:</label>
	<input class = "required email" id ="email" name ="email" type ="text" v/><br />
	<label for ="gender">Gender:</label>
	<select id ="gender" name ="gender" style = "height:30px;">
	    <option>Male</option>
	    <option>Female</option>
	</select><br /><br />
	<label for ="birthdate">Birth Date:</label>
	<input class = "required" style = "width:150px" id ="birthday" name ="birthdate" type ="text" /><br />
	<label>&nbsp;</label> 
	<input type ="submit" value ="Sign Up" name ="submit" id = "submit" class = "button"/>
</form>

</div>