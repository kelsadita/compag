<style type="text/css">
	#captcha{
		width: 500px;
		margin: 80px auto;
		border: 1px solid #CCCCCC;
		padding: 20px;
	}
	
</style>
<div class="linkcontent" >
<?php
				require_once('recaptchalib.php');
				
				//loading session library for storing public key in the session
				$this->load->library('session');
				
				function gen_key($length = 10){
				
					//$length = 10;
					$characters = '123456789abcdefghijklmnopqrstuvwxyz';
					$string = '';    
					for ($p = 0; $p < $length; $p++) 
					{
						$string .= $characters[mt_rand(0, strlen($characters)-1)];
					}
					return $string;
				}
				
				if(isset($_POST['submit']))
				{
				  $privatekey = "6LcPn8wSAAAAAC4GGq8WM23Cc3jLvnNS8rCXkzmF";
				  $resp = recaptcha_check_answer ($privatekey,
				                                $_SERVER["REMOTE_ADDR"],
				                                $_POST["recaptcha_challenge_field"],
				                                $_POST["recaptcha_response_field"]);
				
				  if (!$resp->is_valid) {
				    // What happens when the CAPTCHA was entered incorrectly
				    $notice = "incorrect";
				  } else {
				    // Your code here to handle a successful verification
					
					
					
					do{
						$key_string1 = gen_key();
						$key_string2 = gen_key();
					}while($key_string1 == $key_string2);
					
					$pub_key = sha1($key_string1);
					$private_key = sha1($key_string2);
					
					$added_on = date("Y-m-d H:i:s");
					$this->login_model->add_key($pub_key, $private_key, $added_on);
					$this->login_model->verify($pub_key);	
					
					//------------------------------------------------------------------
					//generating url shortner key...
					do{
						$url_shrt_string = gen_key(5);
						
					}while($this->login_model->chk_collision($url_shrt_string));
					
					//saving url shortner key in database
					$this->login_model->set_urlShrtKey($pub_key, $url_shrt_string);
					
					
					//------------------------------------------------------------------
					
					//setting public key inside the session.
					$key_array = array('pub_key' => $pub_key, 'urlShrtKey' => $url_shrt_string);
					$this->session->set_userdata($key_array);
					
					redirect(base_url().'login/remote_login/'.$private_key);
					
				  }
				}
?>
	<?php if(isset($notice)) echo "<p class=notify>".$notice."</p>"; ?>
	<div id="captcha">
      <form method="post" action="<?php echo base_url(); ?>login/verify">
        <?php
          echo '<center>';
          $publickey = "6LcPn8wSAAAAAFUe86UUA9WHrWRrQ7HDRV6qdgIp"; // you got this from the signup page
          echo recaptcha_get_html($publickey);
          echo '</center>';
        ?>
        <center><input type="submit"  name = "submit" class="button" style="width: 318px;"/></center>
      </form>
      </div>
</div>